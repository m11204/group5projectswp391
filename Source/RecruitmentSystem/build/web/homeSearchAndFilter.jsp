<%-- 
    Document   : home
    Created on : May 28, 2022, 2:33:48 AM
    Author     : huyho
--%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Shop Homepage - Start Bootstrap Template</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>

    <body class="bg-image" 
          style="background-color:#fff0f0;">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-image: url('images/navbar-background.jpg');">
            <div class="container px-lg-5">
                <a class="navbar-brand" href="home">Home</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <form action="search" method="GET" class="d-flex w-50">
                        <input type="hidden" value="${cid}" name="cid" />
                        <input type="hidden" value="${page}" name="page" />
                        <input type="hidden" value="${salary}" name="salary" />
                        <input class="form-control me-2" type="search" placeholder="What's on your mind?" value="${txtSearch}" name="txt">
                        <button class="btn btn-outline-danger" type="submit">Search</button>
                    </form>
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="#">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact</a>
                        </li>
                        <c:choose>
                            <c:when test="${sessionScope.account==null}">
                                <li class="nav-item">
                                    <a class="nav-link" href="login">Login</a>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <c:if test="${sessionScope.account.role != 1}">
                                <li class="nav-item">
                                    <a class="nav-link" href="messenger">Messenger</a>
                                </li>
                                </c:if>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        ${sessionScope.account.displayname}
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <li><a class="dropdown-item" href="profile">Profile</a></li>
                                            <c:if test="${sessionScope.account.role == 1}">
                                            <li><a class="dropdown-item" href="dashboard">Dashboard</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.account.role == 2}">
                                            <li><a class="dropdown-item" href="managementOfRecruiment.jsp">Management</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.account.role == 3}">
                                            <li><a class="dropdown-item" href="manage-cv">CV Manager</a></li>
                                            </c:if>
                                        <li><a class="dropdown-item" href="change-pass">Change password</a></li>
                                        <li><hr class="dropdown-divider"></li>
                                        <li><a class="dropdown-item" href="logout">Log out</a></li>
                                    </ul>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </ul>

                </div>
            </div>
        </nav>
        <!-- Header-->
        <header class="py-5">
            <div class="container px-lg-5">
                <div class="p-4 p-lg-5 bg-light rounded-3 text-center" style="background-image: url('images/navbar-background.jpg');">
                    <div class="m-4 m-lg-5">
                        <h1 class="display-5 fw-bold" >Welcome to Recruitment System</h1>
                        <p class="fs-4">Bootstrap utility classes are used to create this jumbotron since the old component
                            has been removed from the framework. Why create custom CSS when you can use utilities?</p>
                        <a class="btn btn-outline-danger btn-lg" href="#!">Call to action</a>
                    </div>
                </div>
            </div>
        </header>
        <!-- Section-->
        <section class="py-5">
            <div class="container px-4 px-lg-5 mt-5">
                <div class="row">
                    <div class="col-md-4">
                        <form action="search" method="GET">
                            <input type="hidden" value="${txtSearch}" name="txt" />
                            <h3>List Categories</h3> 
                            <select onchange="this.form.submit()" name="cid">
                                <option value="-1">All Categories</option>
                                <c:forEach items="${clist}" var="C">
                                    <option ${C.id == cid?"selected":""} value="${C.id}">${C.name}</option>
                                </c:forEach>
                            </select>


                            <div style="margin-top: 10px">
                                <h3>Salary</h3> 
                                <input ${salary == -1?"checked":""} onclick="this.form.submit()" value="-1" type="radio" name="salary" />All<br/>
                                <input ${salary == 1?"checked":""} onclick="this.form.submit()" type="radio" name="salary" value="1" />less than 1000<br/>
                                <input ${salary == 2?"checked":""} onclick="this.form.submit()" type="radio" name="salary" value="2" />between 1000 and 2000<br/>
                                <input ${salary == 3?"checked":""} onclick="this.form.submit()" type="radio" name="salary" value="3" />more than 2000<br/>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-8">
                        <h3>List Jobs</h3>
                        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                            <c:forEach items="${jlist}" var="J"> 
                                <div class="col mb-5">
                                    ${J.id}
                                    <div class="card h-100">
                                        <!-- Product image-->
                                        <img class="card-img-top" src="https://anhdep.tv/attachments/a739c4cf6d089e96809e1bd84bc41ac4-jpeg.5858/"
                                             alt="..." />
                                        <!-- Product details-->
                                        <div class="card-body p-4">
                                            <div class="text-center">
                                                <!-- Product name-->
                                                <h5 class="fw-bolder">${J.name}</h5>
                                                <!-- Product reviews-->
                                                <div class="d-flex justify-content-center small text-warning mb-2">
                                                    <div class="bi-star-fill"></div>
                                                    <div class="bi-star-fill"></div>
                                                    <div class="bi-star-fill"></div>
                                                    <div class="bi-star-fill"></div>
                                                    <div class="bi-star-fill"></div>
                                                </div>
                                                <!-- Product price-->
                                                <h6>wage  ${J.salary}</h6>
                                                <p>${J.description}</p>
                                                <c:if test="${J.isHot}">
                                                    <span style="background-color: red; color: white">Hot</span><br/>
                                                </c:if>

                                                <c:choose>
                                                    <c:when test="${J.isFulltime}">
                                                        <span style="background-color: green; color: white">Full Time</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <span style="background-color: green; color: white">Part Time</span>
                                                    </c:otherwise>
                                                </c:choose>

                                                <c:if test="${J.isRemote}">
                                                    <span style="background-color: #0dcaf0; color: white">Remote</span><br/>
                                                </c:if>
                                            </div>
                                        </div>
                                        <!-- Product actions-->
                                        <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                            <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="#">View Job details</a></div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>

                        </div>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                <li class="page-item ${page == 1?"disabled":""}">
                                    <a class="page-link" href="search?page=${page - 1}&cid=${cid}&salary=${salary}&txt=${txtSearch}" tabindex="-1" aria-disabled="true">Previous</a>
                                </li>
                                <c:forEach begin="1" end="${totalPage}" var="i">
                                    <li class="page-item ${i == page? "active":""}"><a class="page-link" href="search?page=${i}&cid=${cid}&salary=${salary}&txt=${txtSearch}">${i}</a></li>
                                    </c:forEach>
                                <li class="page-item  ${page == totalPage?"disabled":""}">
                                    <a class="page-link" href="search?page=${page + 1}&cid=${cid}&salary=${salary}&txt=${txtSearch}">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

            </div>
        </section>
        <%@ include file="footer.jsp" %>  
    </body>

</html>
