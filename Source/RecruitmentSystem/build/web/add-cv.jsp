<!--
author: Boostraptheme
author URL: https://boostraptheme.com
License: Creative Commons Attribution 4.0 Unported
License URL: https://creativecommons.org/licenses/by/4.0/
-->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="shortcut icon" href="img/favicon.ico">
        <title>Profile Bootstrap Theme</title>

        <!-- Global stylesheets -->


        <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/devicons/css/devicons.min.css" rel="stylesheet">
        <link href="css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

        <link href="scss/load-more-button.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="js/load-more-button_addCV.js" type="text/javascript"></script>
    </head>

    <body id="page-top" class="bg-image" 
          style="background-color:#fff0f0;">
        
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
            <a href="home" class="btn btn-white">Back to Home</a>
            <a class="navbar-brand js-scroll-trigger" href="#page-top">
                <span class="d-block d-lg-none  mx-0 px-0"><img src="images/logo-white.png" alt="" class="img-fluid"></span>
                <span class="d-none d-lg-block">
                    <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="${sessionScope.account.imageUrl}" alt="">

                </span>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#experience">Experience</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#skills">Skills</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#awards">Awards</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="container-fluid p-0">

            <!--====================================================
                                ABOUT
            ======================================================-->
            <c:if test="${about != null}">
                <section class="resume-section p-3 p-lg-5 d-flex d-column" id="about">
                    <div class="my-auto" >
                        <img src="images/logo-s.png" class="img-fluid mb-3" alt="">
                        <h1 class="mb-0">
                            <span class="text-primary">${about.name}</span>
                        </h1>
                        <div class="subheading mb-5">${about.position}
                        </div>
                        <p class="mb-5" style="max-width: 500px;" >${about.overview}</p>
                        <c:if test="${sessionScope.account.username == about.username}">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                Edit
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="edit-about" method="POST">
                                            <input type="hidden" name="id" value="${about.id}"> 
                                            <input type="hidden" name="type" value="edit"> 
                                            <div class="modal-body">
                                                <div>
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <td>Name:</td>
                                                                <td><input required="" type="text" value="${about.name}" name="name" /><br/></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Job position: </td>
                                                                <td><input required="" type="text" value="${about.position}" name="position" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Overview: </td>
                                                                <td><textarea required="" id="id" placeholder="${about.overview}" name="overview" rows="5" cols="35"></textarea></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </c:if>
                    </div> 
                </section>
            </c:if>

            <c:if test="${about == null}">
                <section class="resume-section p-3 p-lg-5 d-flex d-column" id="about">

                    <div class="my-auto" >
                        <img src="images/logo-s.png" class="img-fluid mb-3" alt="">
                        <div id="loadMore1" style="">
                            <a href="#">Add New</a>
                        </div>
                        <div class="blogBox1 moreBox1" style="display: none">
                            <form action="edit-about" method="POST">
                                <input type="hidden" name="type" value="add"> 
                                <h1 class="mb-0">
                                    <span class="text-primary">Name: <input type="text" name="name" /></span>
                                </h1>
                                <div class="subheading mb-5">Job position:   <input type="text" name="position" />
                                </div>
                                <p class="mb-5" style="max-width: 500px;" >Overview: <textarea id="id" name="overview" rows="5" cols="100"></textarea></p>
                                <button type="submit">Save</button>
                            </form>
                        </div>
                    </div>
                </section>
            </c:if>


            <!--====================================================
                                EXPERIENCE
            ======================================================-->      
            <section class="resume-section p-3 p-lg-5 " id="experience">
                <div class="row my-auto">
                    <div class="col-12">
                        <h2 class="  text-center">Experience</h2>
                        <div class="mb-5 heading-border"></div>
                    </div>

                    <c:forEach items="${experiences}" var="E">
                        <div class="resume-item col-md-6 col-sm-12">
                            <div class="card mx-0 p-4 mb-5" style="border-color: #ffc107; box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.21);">
                                <div class="resume-content mr-auto">
                                    <form action="add-edit-delete-experience" method="POST">
                                        <input type="hidden" name="type" value="delete" />
                                        <input type="hidden" name="id" value="${E.id}" />
                                        <button style="margin-left: 450px" class="alert-danger" href="">Delete</button>
                                    </form>
                                    <h4 class="mb-3"><i class="fa fa-laptop mr-3 text-warning"></i>${E.job}</h4>
                                    <p>${E.description}</p>
                                </div>

                                <div class="resume-date text-md-right">
                                    <span class="text-primary">${E.startDate} - ${E.endDate}</span>
                                </div>
                                <button style="width: 80px" type="button" class="btn btn-primary text-md-left" data-toggle="modal" data-target="#${E.id}">
                                    Edit
                                </button>
                                <div class="modal fade" id="${E.id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <form action="add-edit-delete-experience" method="POST">
                                            <input type="hidden" name="type" value="edit" />
                                            <input type="hidden" name="id" value="${E.id}" />
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <td>Job:</td>
                                                                <td><input required="" type="text" value="${E.job}" name="job" /><br/></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Description: </td>
                                                                <td><input required="" type="text" value="${E.description}" name="description" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>StartDate: </td>
                                                                <td><input required="" type="text" value="${E.startDate}" name="startdate" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>EndDate: </td>
                                                                <td><input required="" type="text" value="${E.endDate}" name="enddate" /></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        </form>
                    </c:forEach>

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModa2">
                        addNew
                    </button>

                    <!-- Modal for add -->
                    <div class="modal fade" id="exampleModa2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <form action="add-edit-delete-experience" method="POST">
                                <input type="hidden" name="type" value="add" />
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Job:</td>
                                                    <td><input required="" type="text" name="job" /><br/></td>
                                                </tr>
                                                <tr>
                                                    <td>Description: </td>
                                                    <td><input required="" type="text" name="description" /></td>
                                                </tr>
                                                <tr>
                                                    <td>StartDate: </td>
                                                    <td><input required="" type="text" name="startdate" /></td>
                                                </tr>
                                                <tr>
                                                    <td>EndDate: </td>
                                                    <td><input required="" type="text" name="enddate" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>   

            <!--====================================================
                                SKILLS
            ======================================================-->       
            <section class=" d-flex flex-column" id="skills">
                <div class="p-lg-5 p-3 skill-cover">
                    <h3 class="text-center text-white">Coding Skills</h3>
                    <div class="row text-center my-auto "> 
                        <c:forEach items="${skills}" var="S">
                            <div class="col-md-3 col-sm-6">
                                <div class="skill-item">
                                    <form action="add-edit-delete-skill" method="POST">
                                        <input type="hidden" name="id" value="${S.id}" />
                                        <input type="hidden" name="type" value="delete" />
                                        <button type="submit" class="bg-danger"><i class="icon-trash"></i></button>
                                    </form>
                                    <h2><span class="counter"> ${S.level} </span><span>%</span></h2>
                                    <p>${S.skill}</p>
                                </div>
                            </div> 
                        </c:forEach>
                    </div>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal3">
                        addNew
                    </button>
                    <!-- Modal for add -->
                    <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <form action="add-edit-delete-skill" method="POST">
                                <input type="hidden" name="type" value="add" />
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Skill:</td>
                                                    <td><input required="" type="text" name="skill" /><br/></td>
                                                </tr>
                                                <tr>
                                                    <td>Level: </td>
                                                    <td><input required="" min="0" max="100" type="number" name="level" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div> 
            </section>

            <!--====================================================
                                   AWARDS
            ======================================================-->       
            <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="awards">
                <div class="row my-auto">
                    <div class="col-12">
                        <h2 class="  text-center">Awards</h2>
                        <div class="mb-5 heading-border"></div>
                    </div> 
                    <div class="main-award" id="award-box">
                        <c:forEach items="${awards}" var="A">
                            <div class="award">
                                <div class="award-icon"></div>
                                <div class="award-content">
                                    <form action="add-edit-delete-award" method="POST">
                                        <input type="hidden" name="type" value="delete" />
                                        <input type="hidden" name="id" value="${A.id}" />
                                        <button class="btn-danger"><i class="icon-trash"></i></button>

                                    </form>

                                    <button style="width: 50px" type="button" class="btn btn-primary text-md-right" data-toggle="modal" data-target="#${A.id}award">
                                        Edit
                                    </button>
                                    <div class="modal fade" id="${A.id}award" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <form action="add-edit-delete-award" method="POST">
                                                <input type="hidden" name="type" value="edit" />
                                                <input type="hidden" name="id" value="${A.id}" />
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                <tr>
                                                                    <td>StartDate: </td>
                                                                    <td><input required="" type="text" value="${A.startDate}" name="startdate" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>EndDate: </td>
                                                                    <td><input required="" type="text" value="${A.endDate}" name="enddate" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Major: </td>
                                                                    <td><input required="" type="text" value="${A.major}" name="major" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Description: </td>
                                                                    <td><input required="" type="text" minlength="30" maxlength="1000" value="${A.description}" name="description" /></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <span class="date">${A.startDate} - ${A.endDate}</span>
                                    <h5 class="title">${A.major}</h5>
                                    <p class="description">
                                        ${A.description}
                                    </p>  
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>

                <!-- Button trigger modal -->
                <button style="width: 100px" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal4">
                    addNew
                </button>
                <!-- Modal for add -->
                <div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <form action="add-edit-delete-award" method="POST">
                            <input type="hidden" name="type" value="add" />
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>StartDate:</td>
                                                <td><input required="" type="text" name="startdate" /><br/></td>
                                            </tr>
                                            <tr>
                                                <td>EndDate:</td>
                                                <td><input required="" type="text" name="enddate" /></td>
                                            </tr>
                                            <tr>
                                                <td>Major: </td>
                                                <td><input required="" type="text" name="major" /></td>
                                            </tr>
                                            <tr>
                                                <td>Description: </td>
                                                <td><textarea required="" minlength="30" maxlength="1000" type="text" name="description" ></textarea></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> 
        </div>
    </section> 

    <!--====================================================
                          CONTACT
    ======================================================-->       
    <section class="resume-section p-3 p-lg-5 d-flex flex-column">
        <div class="row my-auto" id="contact"> 
<!--            <div class="col-md-8">
                <div class="contact-cont">
                    <h3>CONTACT Us</h3>
                    <div class="heading-border-light"></div>
                    <p></p>
                </div>   
                <div class="row con-form">
                    <div class="col-md-12"><textarea name="" id=""></textarea></div>
                    <div class="col-md-12 sub-but"><button class="btn btn-general btn-white" role="button">Send</button></div>
                </div>
            </div>-->
            <div class="col-md-12 col-sm-12 mt-5"> 
                <h3>CONTACT Us</h3>
                <div class="contact-cont2"> 
                    <div class="contact-add contact-box-desc">
                        <h3><i class="fa fa-map-marker cl-atlantis fa-2x"></i> Address</h3>
                        <p>${sessionScope.account.address}<br> <br></p>
                    </div>
                    <div class="contact-phone contact-side-desc contact-box-desc">
                        <h3><i class="fa fa-phone cl-atlantis fa-2x"></i> Phone</h3>
                        <p>${sessionScope.account.phone}</p>
                    </div>
                    <div class="contact-mail contact-side-desc contact-box-desc">
                        <h3><i class="fa fa-envelope-o cl-atlantis fa-2x"></i> Email</h3>
                        <address class="address-details-f"> 
                            Email: <a href="mailto:info@themsbit.com" class="">${sessionScope.account.email}</a>
                        </address>
                    </div>
                </div>
            </div> 
        </div>
    </section>

    <section class=" d-flex flex-column" id="maps">
        <div id="map">
            <div class="map-responsive">
                <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d6030.418742494061!2d-111.34563870463673!3d26.01036670629853!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2smx!4v1471908546569" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </section>


</div>





<!-- Global javascript -->
<script src="js/jquery/jquery.min.js"></script>
<script src="js/bootstrap/bootstrap.bundle.min.js"></script>
<script src="js/jquery-easing/jquery.easing.min.js"></script>
<script src="js/counter/jquery.waypoints.min.js"></script>
<script src="js/counter/jquery.counterup.min.js"></script>
<script src="js/custom.js"></script>
<script>
    $(document).ready(function () {

        $(".filter-b").click(function () {
            var value = $(this).attr('data-filter');
            if (value == "all")
            {
                $('.filter').show('1000');
            } else
            {
                $(".filter").not('.' + value).hide('3000');
                $('.filter').filter('.' + value).show('3000');
            }
        });

        if ($(".filter-b").removeClass("active")) {
            $(this).removeClass("active");
        }
        $(this).addClass("active");
    });

    // SKILLS
    $(function () {
        $('.counter').counterUp({
            delay: 10,
            time: 2000
        });

    });
</script> 
</body>

</html>