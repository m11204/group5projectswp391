<%-- 
    Document   : list-company
    Created on : Jul 5, 2022, 9:36:35 PM
    Author     : huyho
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" integrity="sha256-h20CPZ0QyXlBuAw7A+KluUYx/3pK+c7lYEpqLTlxjYQ=" crossorigin="anonymous" />
    <div class="container">
        <div class="team-single">
            <a class="btn bg-green" href="home"><< Back to Home</a>
            <div class="row">
                <div class="col-lg-4 col-md-5 xs-margin-30px-bottom">
                    <div class="team-single-img">
                        <img src="images/${company.imageURL}" alt="">
                    </div>
                    <div class="bg-light-gray padding-30px-all md-padding-25px-all sm-padding-20px-all text-center">
                        <h4 class="margin-10px-bottom font-size24 md-font-size22 sm-font-size20 font-weight-600">${company.name}</h4>
                        <p class="sm-width-95 sm-margin-auto">Personal Size: ${company.personalSize}</p>
                        <div class="margin-20px-top">
                            <ul class="no-margin list-group" style="list-style-type: none">
                                <li>${company.address}</li>
                                <li>Phone: ${company.phone}</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-7">
                    <div class="team-single-text padding-50px-left sm-no-padding-left">
                        <h4 class="font-size38 sm-font-size32 xs-font-size30">Description</h4>
                        <p class="no-margin-bottom">${company.description}</p>
                        <div class="contact-info-section margin-40px-tb">
                            <h3>Job</h3>
                            <ul class="list-style9 no-margin">
                                <c:forEach items="${jobs}" var="J">
                                    <li>
                                        <div class="row">
                                            <div class="col-md-3 col-3">
                                                <i class="fas text-orange"></i>
                                                <strong class="margin-10px-left text-orange">${J.name}</strong>
                                            </div>
                                            <div class="col-md-3 col-3">
                                                <p>Personal need: ${J.quantity}</p>
                                            </div>
                                            <div class="col-md-3 col-3">
                                                <p>Description: ${J.description}</p>
                                            </div>
                                            <div class="col-md-3 col-3">
                                                <form action="view-job-detail" method="POST">
                                                    <input type="hidden" name="cpid" value="${company.id}"/>
                                                    <input type="hidden" name="jid" value="${J.id}"/>
                                                    <button type="submit">View</button>
                                                </form>
                                            </div>
                                        </div>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">

                </div>
            </div>
        </div>
    </div><script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link href="css/viewdetailcompany.css" rel="stylesheet" type="text/css"/>
</head>
<body>

</body>
</html>
