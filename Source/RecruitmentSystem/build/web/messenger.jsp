<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <%@ include file="meta.jsp" %>
        <title>Recruitment System</title>
        <%@ include file="header.jsp" %>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="scss/load-more-button.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="js/load-more-button.js" type="text/javascript"></script>
    </head>
    <body class="bg-image" 
          style="background-color:#fff0f0;">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-image: url('images/navbar-background.jpg');">
            <div class="container px-lg-5">
                <a class="navbar-brand" href="home">Home</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <form action="search" method="GET" class="d-flex w-50">
                        <input type="hidden" value="${cid}" name="cid" />
                        <input type="hidden" value="${page}" name="page" />
                        <input type="hidden" value="${salary}" name="salary" />
                        <input class="form-control me-2" type="search" placeholder="What's on your mind?" value="${txtSearch}" name="txt">
                        <button class="btn btn-outline-danger" type="submit">Search</button>
                    </form>
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="#">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact</a>
                        </li>
                        <c:choose>
                            <c:when test="${sessionScope.account==null}">
                                <li class="nav-item">
                                    <a class="nav-link" href="login">Login</a>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <c:if test="${sessionScope.account.role != 1}">
                                    <li class="nav-item">
                                        <a class="nav-link" href="messenger">Messenger</a>
                                    </li>
                                </c:if>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        ${sessionScope.account.displayname}
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <li><a class="dropdown-item" href="profile">Profile</a></li>
                                            <c:if test="${sessionScope.account.role == 1}">
                                            <li><a class="dropdown-item" href="dashboard">Dashboard</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.account.role == 2}">
                                            <li><a class="dropdown-item" href="managementOfRecruiment.jsp">Management</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.account.role == 3}">
                                            <li><a class="dropdown-item" href="manage-cv">CV Manager</a></li>
                                            </c:if>
                                        <li><a class="dropdown-item" href="change-pass">Change password</a></li>
                                        <li><hr class="dropdown-divider"></li>
                                        <li><a class="dropdown-item" href="logout">Log out</a></li>
                                    </ul>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </ul>

                </div>
            </div>
        </nav>
        <section style="background-color: #fff0f0;" >
            <div class="container py-5">
                <div class="row">
                    <div class="col-md-6 col-lg-5 col-xl-4 mb-4 mb-md-0">
                        <h5 class="font-weight-bold mb-3 text-center text-lg-start">Member</h5>
                        <div class="card">
                            <div class="card-body">
                                <ul class="list-unstyled mb-0">
                                    <form action="messenger" method="GET"> 
                                        <li class="input-group">
                                            <input type="search" class="form-control rounded" placeholder="Input username" aria-label="Search" value="${usernameSearch}" aria-describedby="search-addon" name="usernamesearch"/>
                                            <button type="submit" class="btn btn-outline-danger">Search</button>
                                        </li>
                                    </form>
                                    <br>


                                    <ul class="nav nav-pills">

                                        <c:forEach items="${acclist}" var="acc">
                                            <li class="p-2 border-bottom nav-item">

                                                <a href="messenger?user=${acc.username}&displayname=${acc.displayname}" class="d-flex justify-content-between nav-link ${chooseUser eq acc.username?"active":""}">
                                                    <div class="d-flex flex-row">
                                                        <img src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/avatar-8.webp" alt="avatar"
                                                             class="rounded-circle d-flex align-self-center me-3 shadow-1-strong" width="60">
                                                        <div  class="pt-1 ">
                                                            <p class="fw-bold mb-0">${acc.displayname}</p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </ul>
                                <nav aria-label="...">
                                    <ul class="pagination">
                                        <li class="page-item ${page == 1 ? "disabled":""}">
                                            <a href="messenger?page=${page - 1}" class="page-link">Previous</a>
                                        </li>
                                        <c:forEach begin="1" end="${totalPage}" var="i">
                                            <li class="page-item"><a class="page-link" href="messenger?page=${i}">${i}</a></li>
                                            </c:forEach>
                                        <li class="page-item ${page == totalPage ? "disabled":""}">
                                            <a class="page-link" href="messenger?page=${page + 1}">Next</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-7 col-xl-8">
                        <ul class="list-unstyled">
                            <div id="loadMore" style="">
                                <a href="#">Load More</a>
                            </div>
                            <br>
                            <c:forEach items="${listMessageOf2People}" var="M">
                                <c:choose>
                                    <c:when test="${M.to eq rusername}">  
                                        <div class="blogBox moreBox" style="display: none;">
                                            <li class="d-flex justify-content-between mb-4 blogBox moreBox" style="display: none;">
                                                <img src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/avatar-5.webp" alt="avatar"
                                                     class="rounded-circle d-flex align-self-start ms-3 shadow-1-strong" width="60">
                                                <div class="card w-100">
                                                    <div class="card-header d-flex justify-content-between p-3">
                                                        <p class="fw-bold mb-0">${displayNameChooseUser}</p>
                                                    </div>
                                                    <div class="card-body">
                                                        <p class="mb-0">
                                                            ${M.content}
                                                        </p>

                                                    </div>
                                                </div>

                                            </li>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="blogBox moreBox" style="display: none">
                                            <li class="d-flex justify-content-between mb-4 blogBox moreBox" style="display: none;">

                                                <div class="card w-100">
                                                    <div class="card-header d-flex justify-content-between p-3">
                                                        <p class="fw-bold mb-0">${sessionScope.account.displayname}</p>
                                                    </div>
                                                    <div class="card-body">
                                                        <p class="mb-0">${M.content}
                                                        </p>
                                                    </div>
                                                </div>
                                                <img src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/avatar-6.webp" alt="avatar"
                                                     class="rounded-circle d-flex align-self-start me-3 shadow-1-strong" width="60">
                                            </li>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                            <form action="messenger" method="POST">
                                <input type="hidden" name="chooseUser" value="${chooseUser}" />
                                <input type="hidden" name="displayname" value="${displayNameChooseUser}" />

                                <input type="hidden" name="usernameSearch" value="${usernameSearch}" />
                                <li class="bg-white mb-3">
                                    <div class="form-outline">
                                        <textarea required="" class="form-control" id="textAreaExample2" name="sendMessage" rows="4"></textarea>
                                        <label class="form-label" for="textAreaExample2">Message</label>
                                    </div>
                                </li>
                                <button type="submit" class="btn btn-info btn-rounded float-end">Send</button>
                            </form>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <%@ include file="footer.jsp" %>  

    </body>

</html>
