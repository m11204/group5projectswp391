<%-- 
    Document   : navbar
    Created on : Jul 5, 2022, 9:16:37 PM
    Author     : huyho
--%>

<nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-image: url('images/navbar-background.jpg');">
    <div class="container px-lg-5">
        <a class="navbar-brand" href="home">Home</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <form action="search" method="GET" class="d-flex w-50">
                <input type="hidden" value="${cid}" name="cid" />
                <input type="hidden" value="${page}" name="page" />
                <input type="hidden" value="${salary}" name="salary" />
                <input class="form-control me-2" type="search" placeholder="What's on your mind?" value="${txtSearch}" name="txt">
                <button class="btn btn-outline-danger" type="submit">Search</button>
            </form>
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <c:if test="${sessionScope.account.role == 3}">
                    <li class="nav-item">
                        <a class="nav-link" href="list-company">Company</a>
                    </li>
                </c:if>
                <c:choose>
                    <c:when test="${sessionScope.account==null}">
                        <li class="nav-item">
                            <a class="nav-link" href="login">Login</a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${sessionScope.account.role != 1}">
                            <li class="nav-item">
                                <a class="nav-link" href="messenger">Messenger</a>
                            </li>
                        </c:if>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                ${sessionScope.account.displayname}
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="profile">Profile</a></li>
                                <c:if test="${sessionScope.account.role == 1}">
                                    <li><a class="dropdown-item" href="dashboard">Dashboard</a></li>
                                </c:if>
                                <c:if test="${sessionScope.account.role == 2}">
                                    <li><a class="dropdown-item" href="managementOfRecruiment.jsp">Management</a></li>
                                </c:if>
                                <c:if test="${sessionScope.account.role == 3}">
                                    <li><a class="dropdown-item" href="manage-cv">CV Manager</a></li>
                                </c:if>
                                <li><a class="dropdown-item" href="change-pass">Change password</a></li>
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="logout">Log out</a></li>
                            </ul>
                        </li>
                    </c:otherwise>
                </c:choose>
            </ul>   

        </div>
    </div>
</nav>