<%-- 
    Document   : addnewcat
    Created on : May 21, 2022, 5:20:10 PM
    Author     : huyho
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="css/stylelogin.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div style="width: 400px; margin: auto">
            <form action="add-new-category" method="POST">
                <h1>Create New Category</h1>
                <h4 style="color: green">${success}</h4>
                <div class="mb-3 mt-3">
                    <label for="cat" class="form-label">Name</label>
                    <input required="" type="text" class="form-control" id="cat" name="name">
                </div>
                <button type="submit" class="btn btn-primary">Add</button>
                <a href="home" class="btn btn-primary">Cancel</a>
            </form>
        </div>
    </body>
</html>
