<%-- 
    Document   : changePassword
    Created on : May 19, 2022, 10:35:18 PM
    Author     : huyho
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="css/stylelogin.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div style="width: 400px; margin: auto">
            <form action="change-pass" method="POST">
                <h1>Change Password</h1>
                <h4 style="color: green">${success}</h4>
                <label for="pwd" class="form-label text-danger">${error1}</label>
                <div class="mb-3 mt-3">
                    <label for="old-pass" class="form-label">Current Password</label>
                    <input required="" type="password" class="form-control" id="old-pass" name="current-pass">
                </div>
                <div class="mb-3">
                    <label for="pwd" class="form-label">New Password</label>
                    <input required="" type="password" class="form-control" id="pwd" name="new-pass">
                </div>
                <div class="mb-3">
                    <label for="cfp" class="form-label">Confirm Password</label>
                    <input required="" type="password" class="form-control" id="cfp" name="confirm-pass">
                </div>
                <label for="pwd" class="form-label text-danger">${error2}</label><br/>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="home" class="btn btn-primary">Cancel</a>
            </form>
        </div>
    </body>
</html>
