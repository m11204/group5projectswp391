<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forget Password</title>
    <link href="https://unpkg.com/tailwindcss@%5E2/dist/tailwind.min.css" rel="stylesheet" />
    <script src="https://unpkg.com/tailwindcss-jit-cdn"></script>
</head>
<body>

    <form action="forgetpassword" method="post">
        <div class="h-full bg-gradient-to-tl from-green-400 to-indigo-900 w-full py-16 px-4" >
            <div class="container mx-auto w-11/12 md:w-2/3 max-w-lg">
                <div class="relative py-8 px-5 md:px-10 bg-white shadow-md rounded-md border border-gray-400">
                    <h1 class="text-gray-800 text-2xl text-center font-bold tracking-normal leading-tight mb-4">Forget Password</h1>
                    <label for="mail" class="text-gray-800 text-sm font-bold leading-tight tracking-normal">Mail</label>
                    <input id="username" required="" name="mail" class="mb-5 mt-2 text-gray-600 focus:outline-none focus:border focus:border-indigo-700 font-normal w-full h-10 flex items-center pl-3 text-sm border-gray-300 rounded border" />
                    
                    
                    <button class="mt-4 w-full bg-gradient-to-tr from-blue-600 to-indigo-600 text-indigo-100 py-2 rounded-md text-lg tracking-wide">Forget Password</button>
                    <p class="text-lg text-center mt-4 text-gray-600"> - OR - </p>
                    <button class="mt-4 w-full bg-gray-100 transition text-center duration-150 text-gray-600 hover:border-gray-400 hover:bg-gray-300 border rounded text-lg tracking-wide py-2" >Cancel</button>
                <h3>${requestScope.mess}</h3>
                </div>
                
            </div>
        </div>
    </form>
</body>
</html>
