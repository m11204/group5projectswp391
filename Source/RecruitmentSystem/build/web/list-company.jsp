<%-- 
    Document   : list-company
    Created on : Jul 5, 2022, 9:36:35 PM
    Author     : huyho
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <link href="css/list-company.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" integrity="sha256-2XFplPlrFClt0bIdPgpz8H7ojnk10H69xRqd9+uTShA=" crossorigin="anonymous" />
        <div class="container mt-3 mb-4">
            <div class="col-lg-9 mt-4 mt-lg-0">
                <div class="row">
                    <div class="col-md-12">
                        <div class="user-dashboard-info-box table-responsive mb-0 bg-white p-4 shadow-sm">
                            <a href="home" class="btn bg-info">Back to Home</a>
                            <table class="table manage-candidates-top mb-0">
                                <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${companys}" var="C">
                                        <tr class="candidates-list">
                                            <td class="title">
                                                <div class="thumb">
                                                    <img class="img-fluid" src="images/${C.imageURL}" alt="">
                                                </div>
                                                <div class="candidate-list-details">
                                                    <div class="candidate-list-info">
                                                        <div class="candidate-list-title">
                                                            <h5 class="mb-0">${C.name}</h5>
                                                        </div>
                                                        <div class="candidate-list-option">
                                                            <ul  class="list-unstyled w-50">
                                                                <li>${C.description}</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                    <form action="view-detail-company" method="POST">
                                        <td class="candidate-list-favourite-time text-center">
                                            <input type="hidden" name="cpid" value="${C.id}">
                                            <button type="submit" class="candidate-list-favourite order-2 text-danger"><i class="fas fa-eye"></i></button><br/>
                                            <span class="candidate-list-time order-1">view</span>
                                        </td>
                                    </form>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-12 text-center mt-4 mt-sm-5">
                                    <ul class="pagination justify-content-center mb-0">
                                        <li class="page-item ${page == 1?"disabled":""}">
                                            <a class="page-link" href="list-company?page=${page - 1}" tabindex="-1" aria-disabled="true">Previous</a>
                                        </li>
                                        <c:forEach begin="1" end="${totalPage}" var="i">
                                            <li class="page-item ${i == page? "active":""}"><a class="page-link" href="list-company?page=${i}">${i}</a></li>
                                            </c:forEach>
                                        <li class="page-item  ${page == totalPage?"disabled":""}">
                                            <a class="page-link" href="list-company?page=${page + 1}">Next</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
