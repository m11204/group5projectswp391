<%-- 
    Document   : list-company
    Created on : Jul 5, 2022, 9:36:35 PM
    Author     : huyho
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" integrity="sha256-h20CPZ0QyXlBuAw7A+KluUYx/3pK+c7lYEpqLTlxjYQ=" crossorigin="anonymous" />
    <div class="container">
        <div class="team-single">
            <div class="row">
                <div style="width: 70%; margin: auto" class="col-lg-12 col-md-12">
                    <div class="team-single-text padding-50px-left sm-no-padding-left">
                        <h4 class="font-size38 sm-font-size32 xs-font-size30 text-green">${job.name}(Salary: ${job.salary}$)</h4>
                        <h4 class="font-size30 sm-font-size24 xs-font-size22 text-dark">${company.name}</h4>
                        <h6 class="font-size20 sm-font-size20 xs-font-size20 text-dark">Expiration date: ${job.expirationDate}</h6>
                        <div class="contact-info-section margin-40px-tb">
                            <ul class="list-style9 mt-5">
                                <li>
                                    <div class="row">
                                        <div class="col-md-12 col-12">
                                            <h3 class="fas text-dark">Work location</h3><br/>
                                            <strong class="margin-10px-left text-dark">-${job.district},${job.province}</strong>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-md-12 col-12">
                                            <h3 class="fas text-dark">Job Description</h3><br/>
                                            <p>-${job.detail}</p>
                                            <h5 class="fas text-dark">Working form: <p class="text-green">${job.isFulltime?"Fulltime":"Partime"},${job.isRemote?"Remote":"Offline"}<p></h5>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <form action="send-request-company" method="POST">
                                    <input type="hidden" name="jid" value="${job.id}" />
                                    <input type="hidden" name="cpid" value="${company.id}" />
                                    <c:if test="${request1 == null}">
                                        <input type="hidden" name="type" value="apply" />
                                        <button class="bg-green btn-success border-0 p-3">Apply now</button>
                                    </c:if>
                                    <c:if test="${request1.status eq 'Pending'}">
                                        <input type="hidden" name="type" value="cancel" />
                                        <button class="bg-danger btn-success border-0 p-3">Cancel Request</button>
                                    </c:if>
                                    <c:if test="${request1.status eq 'Active'}">
                                        <p style="color: green">Your request has been approved</p>
                                    </c:if>
                                </form>
                            </div>
                            <div class="col-md-5">
                                <form action="view-detail-company" method="POST">
                                    <input type="hidden" name="cpid" value="${company.id}"/>
                                    <button class="bg-primary btn-success border-0 p-3" >Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">

                </div>
            </div>
        </div>
    </div><script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link href="css/viewdetailcompany.css" rel="stylesheet" type="text/css"/>
</head>
<body>

</body>
</html>
