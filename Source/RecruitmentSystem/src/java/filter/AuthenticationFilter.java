/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Filter.java to edit this template
 */
package filter;

import dal.AccountDAO;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import model.Account;

/**
 *
 * @author huyho
 */
@WebFilter(filterName = "AuthenticationFilter", urlPatterns = {"/profile","/editprofile"
,"/change-pass","/dashboard","/manager-account","/block-account","/edit-account","/management"
,"/manager-category", "/deactive","/edit-category","/add-new-category","/editrole",
"/approve-account","/edit-job","/delete-job","/add-new-job","/edit-company","/messenger"})
public class AuthenticationFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        HttpSession session = req.getSession();
        //kiem tra dang nhap
        Account account = (Account) session.getAttribute("account");
        if (account != null) {

            //cho qua
            chain.doFilter(request, response);
        } else {
            //check cookie
            //kiem tra cookie
            Cookie[] cookies = req.getCookies();
            String username = null;
            String password = null;
            for (Cookie cooky : cookies) {
                if (cooky.getName().equals("username")) {
                    username = cooky.getValue();
                }
                if (cooky.getName().equals("password")) {
                    password = cooky.getValue();
                }
                if (username != null && password != null) {
                    break;
                }
            }
            if (username != null && password != null) { 
                Account accountLogin = new AccountDAO().checkUsernameAndPass(username, password);
                if(accountLogin != null){ // cookie hop le
                    session.setAttribute("account", accountLogin);
                    chain.doFilter(request, response);
                    return;
                }
            }
            
            //khi ma cookie k ton tai va ca session cung khong
            res.sendRedirect("login");
        }
    }

    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    @Override
    public void init(FilterConfig filterConfig) {
    }
}
