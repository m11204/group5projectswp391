/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.AboutDAO;
import dal.AccountDAO;
import dal.JobDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import model.Account;

/**
 *
 * @author huyho
 */
public class HomeRecruiterController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HomeRecruiterController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet HomeRecruiterController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int page = 1;
        final int PAGE_SIZE = 2;
        String pageStr = request.getParameter("page");
        if (pageStr != null) {
            page = Integer.parseInt(pageStr);
        }
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        ArrayList<String> jobName = new JobDAO().getJobNameByCpid(account.getCpid());
        ArrayList<String> listUsername = new ArrayList<>();
        if (!jobName.isEmpty()) {
            ArrayList<String> listUsername_raw;

            for (String job : jobName) {
                listUsername_raw = new AboutDAO().getAllUsernameValidWithJob(job.substring(0, job.indexOf(" ")).toLowerCase());
                for (String user : listUsername_raw) {
                    listUsername.add(user);
                }
            }
        }
        ArrayList<Account> listAccountsCandidate = new ArrayList<>();
        if (!listUsername.isEmpty()) {
            Map<String, String> userAndJob = new HashMap<>();
            for (String username : listUsername) {
                Account a = new AccountDAO().getAccountByUsername(username);
                if (a != null) {
                    listAccountsCandidate.add(a);
                }
                String job = new AboutDAO().getJobOfUser(username);
                userAndJob.put(username, job);
            }
            int totalPage = listAccountsCandidate.size() / PAGE_SIZE;
            if (totalPage % PAGE_SIZE != 0) {
                totalPage += 1;
            }
            request.setAttribute("totalPage", totalPage);
            request.setAttribute("page", page);
            request.setAttribute("listAccountsCandidate", listAccountsCandidate.subList((page - 1) * PAGE_SIZE, Math.min(page * PAGE_SIZE, listAccountsCandidate.size())));
            request.setAttribute("userAndJob", userAndJob);
        }
        request.getRequestDispatcher("homeRecruiter.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
