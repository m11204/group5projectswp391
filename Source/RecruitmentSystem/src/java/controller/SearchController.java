/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CategoryDAO;
import dal.JobDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Category;
import model.Job;

/**
 *
 * @author admin
 */
public class SearchController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int page = 1;
        int salary = -1;
        final int PAGE_SIZE = 6;
        String txtSearch = "";
        JobDAO jobDAO = new JobDAO();
        int catId = Integer.parseInt(request.getParameter("cid"));
        String salary_raw = request.getParameter("salary");
        String txtSearch_raw = request.getParameter("txt");
        if (txtSearch_raw != null) {
            txtSearch = txtSearch_raw;
        }

        if (salary_raw != null) {
            salary = Integer.parseInt(salary_raw);
        }

        String pageStr = request.getParameter("page");
        if (pageStr != null) {
            page = Integer.parseInt(pageStr);
        }

        int minSalary;
        int maxSalary;
        int totalJobs;
        switch (salary) {
            case -1:
                minSalary = 0;
                maxSalary = Integer.MAX_VALUE;
                break;
            case 1:
                minSalary = 0;
                maxSalary = 999;
                break;
            case 2:
                minSalary = 1000;
                maxSalary = 2000;
                break;
            default:
                minSalary = 2001;
                maxSalary = Integer.MAX_VALUE;
                break;
        }

        if (catId == -1 && salary == -1 && txtSearch.isEmpty()) {
            response.sendRedirect("home");
            return;
        }

        totalJobs = jobDAO.getTotalJobFromCidSalaryAndTxtSearch(minSalary, maxSalary, txtSearch, catId);
        int totalPage = totalJobs / PAGE_SIZE;
        if (totalJobs % PAGE_SIZE != 0) {
            totalPage += 1;
        }

        ArrayList<Job> jlist = jobDAO.getAllJobsByCatIdAndSalaryAndNameWithPagging(page, PAGE_SIZE, catId, minSalary, maxSalary, txtSearch);
        ArrayList<Category> clist = new CategoryDAO().getAllCategories();

        request.setAttribute("salary", salary);
        request.setAttribute("page", page);
        request.setAttribute("cid", catId);
        request.setAttribute("clist", clist);
        request.setAttribute("jlist", jlist);
        request.setAttribute("totalPage", totalPage);
        request.setAttribute("txtSearch", txtSearch);
        request.getRequestDispatcher("homeSearchAndFilter.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//    

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
