/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.AccountDAO;
import dal.CompanyDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Account;
import model.Company;

/**
 *
 * @author huyho
 */
public class EditRoleController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditRoleController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditRoleController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ArrayList<Company> listCompanys = new CompanyDAO().getAllCompany();

        request.setAttribute("listCompanys", listCompanys);
        request.getRequestDispatcher("editrole.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        String option = request.getParameter("option");
        if (option.equals("1")) {
            //neu la company co san thi se update account, status thanh pending va cpid se duoc chuyen thanh id 
            //cua cong ty vua chon
            int cpid = Integer.parseInt(request.getParameter("companyId"));
            
            
            new AccountDAO().requestUpdateRoleAndCpid("Pending",cpid, account.getUsername());
            
            Account a = new AccountDAO().getAccountByUsername(account.getUsername());
            session.setAttribute("account", a);
            response.sendRedirect("profile");
        } else { // company chua ton tai thi van insert 1 company moi voi status la pending
            
            String company_other = request.getParameter("company-other");
            Company c = new CompanyDAO().checkExistCompany(company_other);
            if (c == null) {
                String address = request.getParameter("address");
                String description = request.getParameter("description");
                String phone = request.getParameter("phone");
                String imageUrl = request.getParameter("imageUrl");
                Company company = new Company(0, company_other, address, description, phone, imageUrl,"Pending");
                new CompanyDAO().insertNewCompany(company);
                
                Company cp = new CompanyDAO().getCompanyByCompanyName(company_other);
                
                new AccountDAO().requestUpdateRoleAndCpid("Pending",cp.getId(), account.getUsername());

                Account a = new AccountDAO().getAccountByUsername(account.getUsername());
                session.setAttribute("account", a);
                response.sendRedirect("profile");
            } else {
                ArrayList<Company> listCompanys = new CompanyDAO().getAllCompany();
                request.setAttribute("listCompanys", listCompanys);
                request.setAttribute("error", "Name company is exist");
                request.getRequestDispatcher("editrole.jsp").forward(request, response);
            }

        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
