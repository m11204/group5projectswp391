/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.RequestDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import mail.SendMail;
import model.Request;

/**
 *
 * @author huyho
 */
public class ApproveCancelRequestController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ApproveCancelRequestController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ApproveCancelRequestController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String username = request.getParameter("username");
        int cpid = Integer.parseInt(request.getParameter("cpid"));
        int jId = Integer.parseInt(request.getParameter("jid"));
        new RequestDAO().updateStatusRequest(username,cpid,jId,"Pending");
        response.sendRedirect("http://localhost:8080/RecruitmentSystem/management?type=3");
        
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String username = request.getParameter("username");
        int cpid = Integer.parseInt(request.getParameter("cpid"));
        int jid = Integer.parseInt(request.getParameter("jid"));
        String emailUser = request.getParameter("emailUser");
        String myEmail = request.getParameter("email");
        String myPassEmail = request.getParameter("pass");
        String message = request.getParameter("message");
        new RequestDAO().updateStatusRequest(username,cpid,jid,"Active");
        SendMail.SendMailToCandidate(myEmail,myPassEmail,emailUser,message);
        response.sendRedirect("http://localhost:8080/RecruitmentSystem/management?type=3");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
