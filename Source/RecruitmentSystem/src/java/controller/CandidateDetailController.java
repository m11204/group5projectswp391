/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.AboutDAO;
import dal.AccountDAO;
import dal.AwardDAO;
import dal.ExperienceDAO;
import dal.MessageDAO;
import dal.SkillDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.About;
import model.Account;
import model.Award;
import model.Experience;
import model.Message;
import model.Skill;

/**
 *
 * @author huyho
 */
public class CandidateDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CandidateDetailController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CandidateDetailController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        About about = new AboutDAO().getAboutByUsername(username);
        ArrayList<Experience> experiences = new ExperienceDAO().getAllExperienceByUsername(username);
        ArrayList<Skill> skills = new SkillDAO().getAllSkillByUsername(username);
        Account account = new AccountDAO().getAccountByUsername(username);
        ArrayList<Award> awards = new AwardDAO().getAllAwardsByUsername(username);
        HttpSession session = request.getSession();
        String backOfCV = (String) session.getAttribute("backOfCV");
        if (backOfCV == null) {
            session.setAttribute("backOfCV", "http://localhost:8080/RecruitmentSystem/home-recruiter");
        }

        request.setAttribute("awards", awards);
        request.setAttribute("account", account);
        request.setAttribute("about", about);
        request.setAttribute("experiences", experiences);
        request.setAttribute("skills", skills);
        request.getRequestDispatcher("viewCVCandidate.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account accountRecruiter = (Account) session.getAttribute("account");
        String textMessage = request.getParameter("text-message");
        String username = request.getParameter("username");
        new MessageDAO().addNewMessage(accountRecruiter.getUsername(), username, textMessage);
        About about = new AboutDAO().getAboutByUsername(username);
        ArrayList<Experience> experiences = new ExperienceDAO().getAllExperienceByUsername(username);
        ArrayList<Skill> skills = new SkillDAO().getAllSkillByUsername(username);
        Account account = new AccountDAO().getAccountByUsername(username);
        ArrayList<Award> awards = new AwardDAO().getAllAwardsByUsername(username);

        request.setAttribute("awards", awards);
        request.setAttribute("account", account);
        request.setAttribute("about", about);
        request.setAttribute("experiences", experiences);
        request.setAttribute("skills", skills);

        request.setAttribute("success", "send message success");
        request.getRequestDispatcher("viewCVCandidate.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
