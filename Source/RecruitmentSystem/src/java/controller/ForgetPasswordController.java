/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


import dal.AccountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import mail.JavaMail;
import mail.RandomString;
import model.Account;

/**
 *
 * @author Acer
 */
@WebServlet(name = "ForgetPasswordController", urlPatterns = {"/forgetpassword"})
public class ForgetPasswordController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String mail = request.getParameter("mail");
        String code = request.getParameter("code");
        String codeInContext = (String) request.getServletContext().getAttribute(mail);
        if (code.equals(codeInContext)){
            request.getRequestDispatcher("resetpass.jsp").forward(request, response);
        }else{
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String mail = request.getParameter("mail");
        Account acc =  new  AccountDAO().checkMail(mail);
        if (acc ==null){
            request.setAttribute("mess", "Mail doesn't exist");
        }else{
            String code = RandomString.GenerateRandomCode();
            request.getServletContext().setAttribute(mail, code);
            
            
            LocalDateTime date = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
            String date1 = formatter.format(date);
            request.getServletContext().setAttribute(acc.getUsername(), date1);
 
            
            String url = request.getRequestURL().toString()+"?code="+code+"&mail="+mail;
            request.setAttribute("mess", "Sending success");
            JavaMail jm = new JavaMail();
            jm.sendCode(mail, url);
        }
        request.getRequestDispatcher("forgetpassword.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
