/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.AccountDAO;
import dal.CategoryDAO;
import dal.JobDAO;
import dal.MessageDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import model.Account;
import model.Category;
import model.Job;
import model.Message;

/**
 *
 * @author Dat
 */
public class Messenger extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int page = 1;
        final int PAGE_SIZE = 2;
        String pageStr = request.getParameter("page");
        if (pageStr != null) {
            page = Integer.parseInt(pageStr);
        }

        Account findingacc = new Account();
        ArrayList<Account> acclist = null;
        MessageDAO messageDAO = new MessageDAO();
        AccountDAO accountDAO = new AccountDAO();
        String chooseUser = request.getParameter("user");
        String chooseUserDisplayName = request.getParameter("displayname");
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        String rusername = account.getUsername();

        acclist = messageDAO.getAccountsByRUsernameWithPagging(rusername, page, PAGE_SIZE);
        String usernameSearch = request.getParameter("usernamesearch");
        if (usernameSearch != null) {
            findingacc = accountDAO.checkUserExist(usernameSearch);
            if (findingacc != null) {
                acclist.add(0, findingacc);
            }
        }

        if (chooseUser == null) {

            chooseUser = acclist.get(0).getUsername();
            chooseUserDisplayName = acclist.get(0).getDisplayname();
        }

        ArrayList<Message> listMessageOf2People = messageDAO.getMessageOf2People(account.getUsername(), chooseUser);
        int totalAccount = new MessageDAO().getTotalAccountsByRUsername(rusername);
        int totalPage = totalAccount / PAGE_SIZE;
        if (totalAccount % PAGE_SIZE != 0) {
            totalPage += 1;
        }

        request.setAttribute("totalPage", totalPage);
        request.setAttribute("page", page);
        request.setAttribute("usernameSearch", usernameSearch);
        request.setAttribute("displayNameChooseUser", chooseUserDisplayName);
        request.setAttribute("chooseUser", chooseUser);
        request.setAttribute("listMessageOf2People", listMessageOf2People);
        request.setAttribute("rusername", rusername);
        request.setAttribute("acclist", acclist);
        request.getRequestDispatcher("messenger.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int page = 1;
        final int PAGE_SIZE = 2;
        String pageStr = request.getParameter("page");
        if (pageStr != null) {
            page = Integer.parseInt(pageStr);
        }

        String chooseUser = request.getParameter("chooseUser");
        String sendMessage = request.getParameter("sendMessage");
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        String rusername = account.getUsername();

        new MessageDAO().addNewMessage(rusername, chooseUser, sendMessage);
        // thi insert them 1 truong tin nhan la null de biet thong tin cua ng sap ket noi vs nhau
        String usernameSearch = request.getParameter("usernameSearch");

        ArrayList<Account> acclist = null;
        MessageDAO messageDAO = new MessageDAO();
        AccountDAO accountDAO = new AccountDAO();
        String chooseUserDisplayName = request.getParameter("displayname");

        acclist = messageDAO.getAccountsByRUsernameWithPagging(rusername, page, PAGE_SIZE);

        //if i send message for some one who not send for me any message i will add them to list
        boolean checkFirstMess = messageDAO.checkTheySendMessOrNot(rusername, chooseUser);
        if (!checkFirstMess) {
            new MessageDAO().addNewMessage(chooseUser, rusername, null);
            Account a = new AccountDAO().getAccountByUsername(chooseUser);
            acclist.add(0, a);
        }

        ArrayList<Message> listMessageOf2People = messageDAO.getMessageOf2People(account.getUsername(), chooseUser);
        int totalAccount = new MessageDAO().getTotalAccountsByRUsername(rusername);
        int totalPage = totalAccount / PAGE_SIZE;
        if (totalAccount % PAGE_SIZE != 0) {
            totalPage += 1;
        }

        request.setAttribute("displayNameChooseUser", chooseUserDisplayName);
        request.setAttribute("chooseUser", chooseUser);
        request.setAttribute("listMessageOf2People", listMessageOf2People);
        request.setAttribute("rusername", rusername);
        request.setAttribute("acclist", acclist);
        request.setAttribute("totalPage", totalPage);
        request.setAttribute("page", page);
        request.getRequestDispatcher("messenger.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
