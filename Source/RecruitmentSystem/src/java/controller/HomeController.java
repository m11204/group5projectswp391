/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.AboutDAO;
import dal.AccountDAO;
import dal.CategoryDAO;
import dal.JobDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.About;
import model.Account;
import model.Category;
import model.Job;

/**
 *
 * @author Dat
 */
public class HomeController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int salary = -1;
        int page = 1;
        int cid = -1;
        final int PAGE_SIZE = 6;
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        if (account != null && account.getRole() == 2) {
            response.sendRedirect("home-recruiter");
            return;
        }

        JobDAO jobDAO = new JobDAO();
        String pageStr = request.getParameter("page");
        if (pageStr != null) {
            page = Integer.parseInt(pageStr);
        }
        ArrayList<Category> clist = new CategoryDAO().getAllCategories();

        ArrayList<Job> jlist = jobDAO.getAllJobsWithPagging(page, PAGE_SIZE);
        int totalJobs = jobDAO.getTotalJobs();
        int totalPage = totalJobs / PAGE_SIZE;
        if (totalJobs % PAGE_SIZE != 0) {
            totalPage += 1;
        }

        request.setAttribute("cid", cid);
        request.setAttribute("salary", salary);
        request.setAttribute("page", page);
        request.setAttribute("clist", clist);
        request.setAttribute("jlist", jlist);
        request.setAttribute("totalPage", totalPage);

        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
