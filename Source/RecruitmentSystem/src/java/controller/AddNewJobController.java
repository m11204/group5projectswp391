/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CategoryDAO;
import dal.CompanyDAO;
import dal.JobDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Account;
import model.Category;
import model.Company;
import model.Job;

/**
 *
 * @author huyho
 */
public class AddNewJobController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddNewJobController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddNewJobController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ArrayList<Category> listCategorys = new CategoryDAO().getAllCategories();
        request.setAttribute("listCategorys", listCategorys);
        request.getRequestDispatcher("addnewjob.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String imageUrl = request.getParameter("imageUrl");
        String isFulltime_raw = request.getParameter("isFulltime");
        boolean isFulltime;
        isFulltime = isFulltime_raw.equals("1");
        String province = request.getParameter("province");
        String district = request.getParameter("district");
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        int catId = Integer.parseInt(request.getParameter("categoryId"));
        String detail = request.getParameter("detail");
        String salary_raw = request.getParameter("salary");
        double salary = 0;

        try {
            salary = Double.parseDouble(salary_raw);
        } catch (Exception e) {
            ArrayList<Category> listCategorys = new CategoryDAO().getAllCategories();

            request.setAttribute("name", name);
            request.setAttribute("description", description);
            request.setAttribute("imageUrl", imageUrl);
            request.setAttribute("isFulltime_raw", isFulltime_raw);
            request.setAttribute("province", province);
            request.setAttribute("district", district);
            request.setAttribute("quantity", quantity);
            request.setAttribute("catId", catId);
            request.setAttribute("detail", detail);

            request.setAttribute("error", "INVALID salary");
            request.setAttribute("listCategorys", listCategorys);
            request.getRequestDispatcher("addnewjob.jsp").forward(request, response);
        }

        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        Company c = new CompanyDAO().getCompanyByUsername(account.getUsername());

        Job j = new Job(0, name, c.getId(), description, imageUrl, salary, false, isFulltime, false, province, district, quantity, null, catId, detail);
        new JobDAO().insertJob(j);

        ArrayList<Job> listJobs = new JobDAO().getAllJobsByCompanyIdJoinCat(c.getId());
        request.setAttribute("listJobs", listJobs);
        request.getRequestDispatcher("managerJob.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
