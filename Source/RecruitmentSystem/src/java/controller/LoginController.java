/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.AccountDAO;
import dal.CategoryDAO;
import dal.JobDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Account;
import model.Category;
import model.Job;

/**
 *
 * @author Dat
 */
public class LoginController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //check cookie
        //kiem tra cookie
        Cookie[] cookies = request.getCookies();
        String username = null;
        String password = null;
        for (Cookie cooky : cookies) {
            if (cooky.getName().equals("username")) {
                username = cooky.getValue();
            }
            if (cooky.getName().equals("password")) {
                password = cooky.getValue();
            }
            if (username != null && password != null) {
                break;
            }
        }
        if (username != null && password != null) {
            Account accountLogin = new AccountDAO().checkUsernameAndPass(username, password);
            if (accountLogin != null) { // cookie hop le
                request.getSession().setAttribute("account", accountLogin);
                response.sendRedirect("home");
                return;
            }
        }
        int salary = -1 ;
        int page = 1;
        int cid = -1;
        final int PAGE_SIZE = 6;
        JobDAO jobDAO = new JobDAO();
        String pageStr = request.getParameter("page");
        if (pageStr != null) {
            page = Integer.parseInt(pageStr);
        }
        ArrayList<Category> clist = new CategoryDAO().getAllCategories();

        ArrayList<Job> jlist = jobDAO.getAllJobsWithPagging(page, PAGE_SIZE);
        int totalJobs = jobDAO.getTotalJobs();
        int totalPage = totalJobs / PAGE_SIZE;
        if (totalJobs % PAGE_SIZE != 0) {
            totalPage += 1;
        }

        request.setAttribute("cid", cid);
        request.setAttribute("salary", salary);
        request.setAttribute("page", page);
        request.setAttribute("clist", clist);
        request.setAttribute("jlist", jlist);
        request.setAttribute("totalPage", totalPage);
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    /**x
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String raw_remember = request.getParameter("remember");

        Account a = new AccountDAO().checkUsernameAndPass(username, password);
        if (a != null) {
            if (a.getStatus().equals("Blocked")) {
                request.setAttribute("blocked", "Your account is not allowed to login");
                request.getRequestDispatcher("login.jsp").forward(request, response);
            } else {
                request.getSession().setAttribute("account", a);
                if (raw_remember != null) {
                    Cookie ucookie = new Cookie("username", username);
                    ucookie.setMaxAge(24 * 60 * 60);
                    Cookie pcookie = new Cookie("password", password);
                    pcookie.setMaxAge(24 * 60 * 60);
                    response.addCookie(pcookie);
                    response.addCookie(ucookie);
                }
                response.sendRedirect("home");
            }

        } else {
            request.setAttribute("error", "Invalid username or passsword");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
