/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CategoryDAO;
import dal.CompanyDAO;
import dal.JobDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Account;
import model.Category;
import model.Company;
import model.Job;

/**
 *
 * @author huyho
 */
public class EditJobController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account a = (Account) request.getSession().getAttribute("account");
        int id = Integer.parseInt(request.getParameter("id"));
        Job j = new JobDAO().getJobById(id);
        Company c = new CompanyDAO().getCompanyByUsername(a.getUsername());
        
        ArrayList<Job> listJobs = new JobDAO().getAllJobsByCompanyIdJoinCat(c.getId());
        ArrayList<Category> listCategorys = new CategoryDAO().getAllCategories();

        request.setAttribute("listCategorys", listCategorys);
        request.setAttribute("maxvalue", Integer.MAX_VALUE);
        request.setAttribute("listJobs", listJobs);
        request.setAttribute("job", j);
        request.getRequestDispatcher("editjob.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String salary_raw = request.getParameter("salary");
        int id = Integer.parseInt(request.getParameter("id"));
        double salary = 0;
        try {
            salary = Double.parseDouble(salary_raw);
        } catch (NumberFormatException e) {
            Account a = (Account) request.getSession().getAttribute("account");
            Job j = new JobDAO().getJobById(id);
            Company c = new CompanyDAO().getCompanyByUsername(a.getUsername());
            ArrayList<Job> listJobs = new JobDAO().getAllJobsByCompanyIdJoinCat(c.getId());

            request.setAttribute("listJobs", listJobs);
            request.setAttribute("job", j);

            request.setAttribute("error", "salary is invalid number");
            request.getRequestDispatcher("editjob.jsp").forward(request, response);
        }
        String description = request.getParameter("description");
        String name = request.getParameter("name");
        String imageUrl = request.getParameter("imageUrl");
        String isFulltime_raw = request.getParameter("isFulltime");
        boolean isFulltime;
        isFulltime = isFulltime_raw.equals("1");
        int cid = Integer.parseInt(request.getParameter("categoryId"));
        String province = request.getParameter("province");
        String district = request.getParameter("district");
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        String detail = request.getParameter("detail");
        Job j = new Job(id, name, 0, description, imageUrl, salary, false, isFulltime, false, province, district, quantity, null, cid, detail);
        new JobDAO().updateJobById(j);

        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        Company c = new CompanyDAO().getCompanyByUsername(account.getUsername());
        ArrayList<Job> listJobs = new JobDAO().getAllJobsByCompanyIdJoinCat(c.getId());
        request.setAttribute("listJobs", listJobs);
        request.getRequestDispatcher("managerJob.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
