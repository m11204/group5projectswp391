/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mail;

import java.util.Random;

/**
 *
 * @author hoang
 */
public class RandomString {
    public static String GenerateRandomCode(){
        Random rand = new Random();
        String pool = "qwertyuiopasdfghjklzxcvbnm1234567890";
        String randomPass = null;
        for(int i=0; i<10; i++){
            randomPass += pool.charAt(rand.nextInt(pool.length()));
        }
        return randomPass;
    }
}
