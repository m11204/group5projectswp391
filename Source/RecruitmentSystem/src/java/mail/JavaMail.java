/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mail;
import java.net.PasswordAuthentication;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
/**
 *
 * @author hoang
 */
public class JavaMail {
    private final String mail ="dattqse150059@fpt.edu.vn";
    private final String password = "741751=dat";
    public void SendResetPasswordMail(String emailRec, String passwordRec){
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        
        Session session = Session.getInstance(prop, new Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(mail, password);
            }   
        });
        try {
            Message mess = ForgetPasswordMail(session, emailRec, passwordRec);
            Transport.send(mess);
            System.out.println("Success");
        } catch (MessagingException ex) {
            Logger.getLogger(JavaMail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e){
            
        }
            
    }
    
    public void SendFeedbackMail(String emailRec, String content){
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        
        Session session = Session.getInstance(prop, new Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(mail, password);
            }   
        });
        try {
            Message mess = FeedbackMail(session, emailRec, content);
            Transport.send(mess);
            System.out.println("Success");
        } catch (MessagingException ex) {
            Logger.getLogger(JavaMail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e){
            
        }
            
    }
    
    private  Message FeedbackMail(Session session, String reciever, String content){
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("ducchhe150151@fpt.edu.vn"));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(reciever));
            message.setSubject("Feedback from user");
            message.setText("There is a feed back from user,\nContent: "+ content);
            return message;
        } catch (AddressException ex) {
            Logger.getLogger(JavaMail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e){
            
        }
        return null;
    }
    
    private  Message ForgetPasswordMail(Session session, String reciever, String content){
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("ducchhe150151@fpt.edu.vn"));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(reciever));
            message.setSubject("[Change Password Request From Fvax]");
            message.setText("Greetings,\nYour new password is: "+ content);
            return message;
        } catch (AddressException ex) {
            Logger.getLogger(JavaMail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e){
            
        }
        return null;
    }
    public void sendCode(String emailRec, String link){
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        
        Session session = Session.getInstance(prop, new Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(mail, password);
            }   
        });
        try {
            Message mess = CodeMail(session, emailRec, link);
            Transport.send(mess);
            System.out.println("Success");
        } catch (MessagingException ex) {
            Logger.getLogger(JavaMail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e){
            
        }
            
    }
    private  Message CodeMail(Session session, String reciever, String content){
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(mail));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(reciever));
            message.setSubject("Here's your link, click to reset password!");
            message.setText(content);
            return message;
        } catch (AddressException ex) {
            Logger.getLogger(JavaMail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e){
            
        }
        return null;
    }
    
    public static void main(String[] args) {
        JavaMail jm = new JavaMail();
        try{
            jm.sendCode("tqdatqn01230@gmail.com", "ABC");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
