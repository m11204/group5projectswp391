/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Award;
import model.Experience;

/**
 *
 * @author huyho
 */
public class AwardDAO {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;

    public ArrayList<Award> getAllAwardsByUsername(String username) {
        ArrayList<Award> list = new ArrayList<>();
        try {

            String sql = "SELECT TOP (1000) [id]\n"
                    + "      ,[startdate]\n"
                    + "      ,[enddate]\n"
                    + "      ,[major]\n"
                    + "      ,[description]\n"
                    + "      ,[username]\n"
                    + "  FROM [RecruimentSystem].[dbo].[Awards] where username = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                Award a = new Award(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
                list.add(a);
            }
        } catch (Exception ex) {
            Logger.getLogger(ExperienceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public void addNewAward(String startdate, String endDate, String major, String description, String username) {
        try {
            String sql = "INSERT INTO [dbo].[Awards]\n"
                    + "           ([startdate]\n"
                    + "           ,[enddate]\n"
                    + "           ,[major]\n"
                    + "           ,[description]\n"
                    + "           ,[username])\n"
                    + "     VALUES\n"
                    + "           (?,?,?,?,?)";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, startdate);
            ps.setString(2, endDate);
            ps.setString(3, major);
            ps.setString(4, description);
            ps.setString(5, username);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(AwardDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteAwardFromId(int id) {
        try {
            String sql = "DELETE FROM [dbo].[Awards]\n"
                    + "      WHERE id = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(AwardDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void UpdateAward(String startdate, String endDate, String major, String description, int id) {
        try {
            String sql = "UPDATE [dbo].[Awards]\n"
                    + "   SET [startdate] = ?\n"
                    + "      ,[enddate] = ?\n"
                    + "      ,[major] = ?\n"
                    + "      ,[description] = ?\n"
                    + " WHERE id = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, startdate);
            ps.setString(2, endDate);
            ps.setString(3, major);
            ps.setString(4, description);
            ps.setInt(5, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(AwardDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
