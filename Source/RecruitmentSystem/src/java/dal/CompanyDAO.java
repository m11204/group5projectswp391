/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Company;
import model.Job;

/**
 *
 * @author huyho
 */
public class CompanyDAO {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;

    //get all Company from db
    public ArrayList<Company> getAllCompany() {
        ArrayList<Company> list = new ArrayList<>();
        try {
            String sql = "select * from Company";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Company c = new Company(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7));
                list.add(c);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public void insertNewCompany(Company company) {
        try {
            String sql = "INSERT INTO [dbo].[Company]\n"
                    + "           ([name]\n"
                    + "           ,[address]\n"
                    + "           ,[description]\n"
                    + "           ,[phone]\n"
                    + "           ,[imageUrl]\n"
                    + "           ,[status])\n"
                    + "     VALUES\n"
                    + "           (?,?,?,?,?,?)";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, company.getName());
            ps.setString(2, company.getAddress());
            ps.setString(3, company.getDescription());
            ps.setString(4, company.getPhone());
            ps.setString(5, company.getImageURL());
            ps.setString(6, company.getStatus());
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Company checkExistCompany(String company_other) {
        try {
            String sql = " select * from Company where name = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, company_other);
            rs = ps.executeQuery();
            if (rs.next()) {
                Company c = new Company(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7));
                return c;
            }
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void updateRecruiterOfCompany(Company c, String recruiter) {
        try {
            String sql = "UPDATE [dbo].[Company]\n"
                    + "   SET [recruiter] = ?\n"
                    + " WHERE id = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, recruiter);
            ps.setInt(2, c.getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteCompanyPending(Company c) {
        try {
            String sql = "DELETE FROM [dbo].[Company]\n"
                    + "      WHERE id = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, c.getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateStatusOfCompany(Company c, String active) {
        try {
            String sql = "UPDATE [dbo].[Company]\n"
                    + "   SET [status] = ?\n"
                    + " WHERE id = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, active);
            ps.setInt(2, c.getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void UpdateCompany(Company c) {
        try {
            String sql = "UPDATE [dbo].[Company]\n"
                    + "   SET [name] = ?\n"
                    + "      ,[address] = ?\n"
                    + "      ,[description] = ?\n"
                    + "      ,[phone] = ?\n"
                    + "      ,[imageUrl] = ?\n"
                    + "      ,[status] = ?\n"
                    + "      ,[personalsize] = ?\n"
                    + " WHERE id = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, c.getName());
            ps.setString(2, c.getAddress());
            ps.setString(3, c.getDescription());
            ps.setString(4, c.getPhone());
            ps.setString(5, c.getImageURL());
            ps.setString(6, c.getStatus());
            ps.setInt(7, c.getPersonalSize());
            ps.setInt(8, c.getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Company getCompanyByCompanyName(String company_other) {
        try {
            String sql = "select * from Company where name = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, company_other);
            rs = ps.executeQuery();
            while (rs.next()) {
                Company c = new Company(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7));
                return c;
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Company getCompanyByUsername(String username) {
        try {
            String sql = "SELECT Company.*\n"
                    + "  FROM [dbo].[Account] inner join Company on Account.cpid = Company.id\n"
                    + "  where username = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                Company c = new Company(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7), rs.getInt(8));
                return c;
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Company getCompanyById(int cpid) {
        try {
            String sql = "SELECT Company.*\n"
                    + "  FROM [dbo].[Account] inner join Company on Account.cpid = Company.id\n"
                    + "  where id = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, cpid);
            rs = ps.executeQuery();
            while (rs.next()) {
                Company c = new Company(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7), rs.getInt(8));
                return c;
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
