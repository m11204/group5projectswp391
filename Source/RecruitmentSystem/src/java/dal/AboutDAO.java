/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBContext;
import jakarta.servlet.jsp.jstl.sql.Result;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.About;

/**
 *
 * @author huyho
 */
public class AboutDAO {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;

    public About getAboutByUsername(String username) {
        try {
            String sql = "SELECT [id]\n"
                    + "      ,[name]\n"
                    + "      ,[position]\n"
                    + "      ,[overview]\n"
                    + "      ,[username]\n"
                    + "      ,[status]\n"
                    + "  FROM [dbo].[About] where username = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            if (rs.next()) {
                About a = new About(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6));
                return a;
            }
        } catch (Exception ex) {
            Logger.getLogger(AboutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void updateAbout(int id, String name, String position, String overview) {
        try {
            String sql = "UPDATE [dbo].[About]\n"
                    + "   SET [name] = ?\n"
                    + "      ,[position] = ?\n"
                    + "      ,[overview] = ?\n"
                    + " WHERE id = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, position);
            ps.setString(3, overview);
            ps.setInt(4, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(AboutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addNewAbout(String name, String position, String overview, String username) {
        try {
            String sql = "INSERT INTO [dbo].[About]\n"
                    + "           ([name]\n"
                    + "           ,[position]\n"
                    + "           ,[overview]\n"
                    + "           ,[username])\n"
                    + "     VALUES\n"
                    + "           (?,?,?,?)";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, position);
            ps.setString(3, overview);
            ps.setString(4, username);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(AboutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getUsernameValidWithCompany(String job) {
        try {
            String sql = " select username from About where position like ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + job + "%");
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (Exception ex) {
            Logger.getLogger(AboutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String getJobOfUser(String username) {
        try {
            String sql = "SELECT [position]\n"
                    + "  FROM [RecruimentSystem].[dbo].[About] where username = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (Exception ex) {
            Logger.getLogger(AboutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<String> getAllUsernameValidWithJob(String job) {
        ArrayList<String> list = new ArrayList<>();
        try {
            String sql = "select username from About where position like ? and status = 1 order by publicDate";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + job + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(rs.getString(1));
            }
        } catch (Exception ex) {
            Logger.getLogger(AboutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public void UpdateStatus(int status, String username) {
        try {
            String sql = "UPDATE [dbo].[About]\n"
                    + "   SET [status] = ?\n"
                    + "      ,[publicDate] = getDate()\n"
                    + " WHERE username = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, status);
            ps.setString(2, username);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(AboutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
