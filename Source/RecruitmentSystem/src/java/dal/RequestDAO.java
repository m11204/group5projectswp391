/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Company;
import model.Job;
import model.Request;

/**
 *
 * @author huyho
 */
public class RequestDAO {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;

    public void insertRequest(String username, int cpid, int jid, String status) {
        try {
            String sql = "INSERT INTO [dbo].[Request]\n"
                    + "           ([username]\n"
                    + "           ,[cpid]\n"
                    + "           ,[jid]\n"
                    + "           ,[status])\n"
                    + "     VALUES\n"
                    + "           (?,?,?,?)";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setInt(2, cpid);
            ps.setInt(3, jid);
            ps.setString(4, status);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Request getReqByCpidJidAndUsername(String username, int cpid, int jid) {
        try {
            String sql = "SELECT [username]\n"
                    + "      ,[cpid]\n"
                    + "      ,[jid]\n"
                    + "      ,[status]\n"
                    + "  FROM [dbo].[Request] where username = ? and cpid = ? and jid = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setInt(2, cpid);
            ps.setInt(3, jid);
            rs = ps.executeQuery();
            if (rs.next()) {
                Request request = new Request(rs.getString(1), rs.getInt(2), rs.getInt(3),
                        rs.getString(4));
                return request;
            }
        } catch (Exception ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void deleteRequest(String username, int cpid, int jid) {
        try {
            String sql = "DELETE FROM [dbo].[Request]\n"
                    + "      WHERE username = ? and cpid=? and jid = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setInt(2, cpid);
            ps.setInt(3, jid);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Request> getAllRequests() {
        ArrayList<Request> list = new ArrayList<>();
        try {
            String sql = "SELECT r.[username],r.[cpid],r.[jid],r.[status],c.id,c.name,c.address,c.description,c.phone,c.imageUrl,c.status,c.personalsize,\n"
                    + "  j.id,j.name,j.cpid,j.shortdescription,j.imageUrl,j.salary,j.isHot,j.isFulltime,j.isRemote,j.province,j.district,j.quantity,j.expirationdate,j.cid\n"
                    + "  ,j.detail,a.username,a.displayname,a.gender,a.email,a.phone,a.address,a.role,a.imageUrl,a.status\n"
                    + "  FROM [dbo].[Request] r join Company c\n"
                    + "  on c.id = r.cpid\n"
                    + "  join Job j on r.jid = j.id\n"
                    + "  join Account a on r.username = a.username";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Company company = new Company(rs.getInt(5), rs.getString(6),
                        rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getString(10),
                        rs.getString(11), rs.getInt(12));
                Job job = new Job(rs.getInt(13), rs.getString(14),
                        rs.getInt(15),
                        rs.getString(16),
                        rs.getString(17),
                        rs.getDouble(18),
                        rs.getBoolean(19),
                        rs.getBoolean(20),
                        rs.getBoolean(21),
                        rs.getString(22),
                        rs.getString(23),
                        rs.getInt(24),
                        rs.getDate(25),
                        rs.getInt(26),
                        rs.getString(27));
                Account account = new Account(rs.getString(28), null, rs.getString(29), rs.getInt(30),
                        rs.getString(31),
                        rs.getString(32),
                        rs.getString(33),
                        rs.getInt(34),
                        rs.getString(35),
                        0, rs.getString(36));
                Request request = new Request(rs.getString(1), rs.getInt(2), rs.getInt(3), rs.getString(4), job, company, account);
                list.add(request);
            }
        } catch (Exception ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public void updateStatusRequest(String username, int cpid, int jId, String status) {
        try {
            String sql = "UPDATE [dbo].[Request]\n"
                    + "   SET [status] = ?\n"
                    + " WHERE username = ? and cpid = ? and jid=?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, status);
            ps.setString(2, username);
            ps.setInt(3, cpid);
            ps.setInt(4, jId);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
