/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Experience;

/**
 *
 * @author huyho
 */
public class ExperienceDAO {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;

    public ArrayList<Experience> getAllExperienceByUsername(String username) {
        ArrayList<Experience> list = new ArrayList<>();
        try {

            String sql = "SELECT [id]\n"
                    + "      ,[job]\n"
                    + "      ,[description]\n"
                    + "      ,[startdate]\n"
                    + "      ,[enddate]\n"
                    + "      ,[username]\n"
                    + "  FROM [dbo].[Experience] where username = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                Experience e = new Experience(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
                        rs.getString(6));
                list.add(e);
            }
        } catch (Exception ex) {
            Logger.getLogger(ExperienceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public void addNewExperience(String job, String description, String startdate, String enddate, String username) {
        try {
            String sql = "INSERT INTO [dbo].[Experience]\n"
                    + "           ([job]\n"
                    + "           ,[description]\n"
                    + "           ,[startdate]\n"
                    + "           ,[enddate]\n"
                    + "           ,[username])\n"
                    + "     VALUES\n"
                    + "           (?,?,?,?,?)";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, job);
            ps.setString(2, description);
            ps.setString(3, startdate);
            ps.setString(4, enddate);
            ps.setString(5, username);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(ExperienceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editExperience(String job, String description, String startdate, String enddate, int id) {
        try {
            String sql = "UPDATE [dbo].[Experience]\n"
                    + "   SET [job] = ?\n"
                    + "      ,[description] = ?\n"
                    + "      ,[startdate] = ?\n"
                    + "      ,[enddate] = ?\n"
                    + " WHERE id = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, job);
            ps.setString(2, description);
            ps.setString(3, startdate);
            ps.setString(4, enddate);
            ps.setInt(5, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(ExperienceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void DeleteExperience(int id) {
        try {
            String sql = "DELETE FROM [dbo].[Experience]\n"
                    + "      WHERE id = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(ExperienceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
