/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Job;

/**
 *
 * @author Dat
 */
public class JobDAO {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;

    public ArrayList<Job> getAllJobs() {
        ArrayList<Job> list = new ArrayList<>();
        try {
            String sql = "select * from Job";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Job j = new Job(rs.getInt(1), rs.getString(2), rs.getInt(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getBoolean(7),
                        rs.getBoolean(8),
                        rs.getBoolean(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getInt(12),
                        rs.getDate(13),
                        rs.getInt(14),
                        rs.getString(15));
                list.add(j);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public ArrayList<Job> getAllJobsWithPagging(int page, int PAGE_SIZE) {
        ArrayList<Job> list = new ArrayList<>();
        try {
            String sql = "select * from Job order by id offset (?-1)*? row fetch next ? rows only";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, page);
            ps.setInt(2, PAGE_SIZE);
            ps.setInt(3, PAGE_SIZE);
            rs = ps.executeQuery();
            while (rs.next()) {
                Job j = new Job(rs.getInt(1), rs.getString(2), rs.getInt(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getBoolean(7),
                        rs.getBoolean(8),
                        rs.getBoolean(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getInt(12),
                        rs.getDate(13),
                        rs.getInt(14),
                        rs.getString(15));
                list.add(j);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public int getTotalJobs() {
        try {
            String sql = "select count(id) from Job ";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getTotalJobsByCatIdAndSalary(int catId, int minSalary, int maxSalary) {
        try {
            String sql = "select count(id) from Job where cid = ? and salary between ? and ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, catId);
            ps.setInt(2, minSalary);
            ps.setInt(3, maxSalary);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public ArrayList<Job> getAllJobsByCatIdAndSalaryWithPagging(int page, int PAGE_SIZE, int catId, int minSalary, int maxSalary) {
        ArrayList<Job> list = new ArrayList<>();
        try {
            String sql = "select * from Job where cid = ? and salary between ? and ? order by id offset (?-1)*? row fetch next ? rows only";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, catId);
            ps.setInt(2, minSalary);
            ps.setInt(3, maxSalary);

            ps.setInt(4, page);
            ps.setInt(5, PAGE_SIZE);
            ps.setInt(6, PAGE_SIZE);
            rs = ps.executeQuery();
            while (rs.next()) {
                Job j = new Job(rs.getInt(1), rs.getString(2), rs.getInt(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getBoolean(7),
                        rs.getBoolean(8),
                        rs.getBoolean(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getInt(12),
                        rs.getDate(13),
                        rs.getInt(14),
                        rs.getString(15));
                list.add(j);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public int getTotalJobsBySalary(int minSalary, int maxSalary) {
        try {
            String sql = "select count(id) from Job where salary between ? and ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, minSalary);
            ps.setInt(2, maxSalary);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public ArrayList<Job> getAllJobsBySalaryWithPagging(int page, int PAGE_SIZE, int minSalary, int maxSalary) {
        ArrayList<Job> list = new ArrayList<>();
        try {
            String sql = "select * from Job where salary between ? and ? order by id offset (?-1)*? row fetch next ? rows only";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, minSalary);
            ps.setInt(2, maxSalary);

            ps.setInt(3, page);
            ps.setInt(4, PAGE_SIZE);
            ps.setInt(5, PAGE_SIZE);
            rs = ps.executeQuery();
            while (rs.next()) {
                Job j = new Job(rs.getInt(1), rs.getString(2), rs.getInt(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getBoolean(7),
                        rs.getBoolean(8),
                        rs.getBoolean(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getInt(12),
                        rs.getDate(13),
                        rs.getInt(14),
                        rs.getString(15));
                list.add(j);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public ArrayList<Job> searchJobsByName(String txtSearch) {
        ArrayList<Job> list = new ArrayList<>();
        try {
            String sql = "select * from Job\n"
                    + "where [name] like ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + txtSearch + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Job j = new Job(rs.getInt(1), rs.getString(2), rs.getInt(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getBoolean(7),
                        rs.getBoolean(8),
                        rs.getBoolean(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getInt(12),
                        rs.getDate(13),
                        rs.getInt(14),
                        rs.getString(15));
                list.add(j);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public int getTotalJobFromCidSalaryAndTxtSearch(int minSalary, int maxSalary, String txtSearch, int catId) {

        try {
            String sql = "select count(id) from Job where [name] like ? and salary between ? and ?";
            if (catId != -1) {
                sql += " and cid = " + catId;
            }
            conn = new DBContext().getConnection();

            ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + txtSearch + "%");
            ps.setInt(2, minSalary);
            ps.setInt(3, maxSalary);

            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public ArrayList<Job> getAllJobsFromNameWithPagging(int page, int PAGE_SIZE, String txtSearch) {
        ArrayList<Job> list = new ArrayList<>();
        try {
            String sql = "select * from Job where name like ? order by id offset (?-1)*? row fetch next ? rows only";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + txtSearch + "%");
            ps.setInt(2, page);
            ps.setInt(3, PAGE_SIZE);
            ps.setInt(4, PAGE_SIZE);
            rs = ps.executeQuery();
            while (rs.next()) {
                Job j = new Job(rs.getInt(1), rs.getString(2), rs.getInt(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getBoolean(7),
                        rs.getBoolean(8),
                        rs.getBoolean(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getInt(12),
                        rs.getDate(13),
                        rs.getInt(14),
                        rs.getString(15));
                list.add(j);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public ArrayList<Job> getAllJobsBySalaryAndFromJobWithPagging(int page, int PAGE_SIZE, int minSalary, int maxSalary, String txtSearch) {
        ArrayList<Job> list = new ArrayList<>();
        try {
            String sql = "select * from Job where name like ? and salary between ? and ? order by id offset (?-1)*? row fetch next ? rows only";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + txtSearch + "%");
            ps.setInt(2, minSalary);
            ps.setInt(3, maxSalary);
            ps.setInt(4, page);
            ps.setInt(5, PAGE_SIZE);
            ps.setInt(6, PAGE_SIZE);
            rs = ps.executeQuery();
            while (rs.next()) {
                Job j = new Job(rs.getInt(1), rs.getString(2), rs.getInt(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getBoolean(7),
                        rs.getBoolean(8),
                        rs.getBoolean(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getInt(12),
                        rs.getDate(13),
                        rs.getInt(14),
                        rs.getString(15));
                list.add(j);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public ArrayList<Job> getAllJobsByCatIdAndFromJobWithPagging(int page, int PAGE_SIZE, int catId, String txtSearch) {
        ArrayList<Job> list = new ArrayList<>();
        try {
            String sql = "select * from Job where name like ? and cid = ? order by id offset (?-1)*? row fetch next ? rows only";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + txtSearch + "%");
            ps.setInt(2, catId);
            ps.setInt(3, page);
            ps.setInt(4, PAGE_SIZE);
            ps.setInt(5, PAGE_SIZE);
            rs = ps.executeQuery();
            while (rs.next()) {
                Job j = new Job(rs.getInt(1), rs.getString(2), rs.getInt(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getBoolean(7),
                        rs.getBoolean(8),
                        rs.getBoolean(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getInt(12),
                        rs.getDate(13),
                        rs.getInt(14),
                        rs.getString(15));
                list.add(j);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public ArrayList<Job> getAllJobsByCatIdAndSalaryAndNameWithPagging(int page, int PAGE_SIZE, int catId, int minSalary, int maxSalary, String txtSearch) {
        ArrayList<Job> list = new ArrayList<>();
        try {
            String sql = "select * from Job where salary between ? and ? and name like ?";
            if (catId != -1) {
                sql += " and cid = " + catId + " order by id offset (?-1)*? row fetch next ? rows only";
            } else {
                sql += " order by id offset (?-1)*? row fetch next ? rows only";
            }
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, minSalary);
            ps.setInt(2, maxSalary);
            ps.setString(3, "%" + txtSearch + "%");
            ps.setInt(4, page);
            ps.setInt(5, PAGE_SIZE);
            ps.setInt(6, PAGE_SIZE);
            rs = ps.executeQuery();
            while (rs.next()) {
                Job j = new Job(rs.getInt(1), rs.getString(2), rs.getInt(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getBoolean(7),
                        rs.getBoolean(8),
                        rs.getBoolean(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getInt(12),
                        rs.getDate(13),
                        rs.getInt(14),
                        rs.getString(15));
                list.add(j);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public ArrayList<Job> getAllJobsByCompanyIdJoinCat(int id) {
        ArrayList<Job> list = new ArrayList<>();
        try {
            String sql = "select Job.*, Category.name from job join Category on Job.cid = Category.id where cpid = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Job j = new Job(rs.getInt(1), rs.getString(2), rs.getInt(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getBoolean(7),
                        rs.getBoolean(8),
                        rs.getBoolean(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getInt(12),
                        rs.getDate(13),
                        rs.getInt(14),
                        rs.getString(15),
                        rs.getString(16));
                list.add(j);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public Job getJobById(int id) {
        try {
            String sql = "select * from job where id = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Job j = new Job(rs.getInt(1), rs.getString(2), rs.getInt(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getBoolean(7),
                        rs.getBoolean(8),
                        rs.getBoolean(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getInt(12),
                        rs.getDate(13),
                        rs.getInt(14),
                        rs.getString(15));
                return j;
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void updateJobById(Job j) {
        try {
            String sql = "UPDATE [dbo].[Job]\n"
                    + "   SET [name] = ?\n"
                    + "      ,[shortdescription] = ?\n"
                    + "      ,[imageUrl] = ?\n"
                    + "      ,[salary] = ?\n"
                    + "      ,[isFulltime] = ?\n"
                    + "      ,[province] = ?\n"
                    + "      ,[district] = ?\n"
                    + "      ,[quantity] = ?\n"
                    + "      ,[cid] = ?\n"
                    + "      ,[detail] = ?\n"
                    + " WHERE id = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, j.getName());
            ps.setString(2, j.getDescription());
            ps.setString(3, j.getImageUrl());
            ps.setDouble(4, j.getSalary());
            ps.setBoolean(5, j.isIsFulltime());
            ps.setString(6, j.getProvince());
            ps.setString(7, j.getDistrict());
            ps.setInt(8, j.getQuantity());
            ps.setInt(9, j.getCid());
            ps.setString(10, j.getDetail());
            ps.setInt(11, j.getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void DeleteJobById(int id) {
        try {
            String sql = "DELETE FROM [dbo].[Job]\n"
                    + "      WHERE id = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void insertJob(Job j) {
        try {
            String sql = "INSERT INTO [dbo].[Job]\n"
                    + "           ([name]\n"
                    + "           ,[cpid]\n"
                    + "           ,[shortdescription]\n"
                    + "           ,[imageUrl]\n"
                    + "           ,[salary]\n"
                    + "           ,[isFulltime]\n"
                    + "           ,[province]\n"
                    + "           ,[district]\n"
                    + "           ,[quantity]\n"
                    + "           ,[cid]\n"
                    + "           ,[detail])\n"
                    + "     VALUES\n"
                    + "           (?,?,?,?,?,?,?,?,?,?,?)";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, j.getName());
            ps.setInt(2, j.getCpid());
            ps.setString(3, j.getDescription());
            ps.setString(4, j.getImageUrl());
            ps.setDouble(5, j.getSalary());
            ps.setBoolean(6, j.isIsFulltime());
            ps.setString(7, j.getProvince());
            ps.setString(8, j.getDistrict());
            ps.setInt(9, j.getQuantity());
            ps.setInt(10, j.getCid());
            ps.setString(11, j.getDetail());
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<String> getJobNameByCpid(int cpid) {
        ArrayList<String> list = new ArrayList<>();
        try {
            String sql = "select [name] from Job where cpid = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, cpid);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(rs.getString(1));
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public ArrayList<Job> getAllJobByCpid(int cpid) {
        ArrayList<Job> list = new ArrayList<>();
        try {
            String sql = "SELECT [id]\n"
                    + "      ,[name]\n"
                    + "      ,[cpid]\n"
                    + "      ,[shortdescription]\n"
                    + "      ,[imageUrl]\n"
                    + "      ,[salary]\n"
                    + "      ,[isHot]\n"
                    + "      ,[isFulltime]\n"
                    + "      ,[isRemote]\n"
                    + "      ,[province]\n"
                    + "      ,[district]\n"
                    + "      ,[quantity]\n"
                    + "      ,[expirationdate]\n"
                    + "      ,[cid]\n"
                    + "      ,[detail]\n"
                    + "  FROM [dbo].[Job] where cpid = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, cpid);
            rs = ps.executeQuery();
            while (rs.next()) {
                Job job = new Job(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getString(5), rs.getDouble(6),
                         rs.getBoolean(7), rs.getBoolean(8),
                         rs.getBoolean(9), rs.getString(10), rs.getString(11),
                         rs.getInt(12), rs.getDate(13),
                         rs.getInt(14), rs.getString(15));
                list.add(job);
            }
        } catch (Exception ex) {
            Logger.getLogger(JobDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

}
