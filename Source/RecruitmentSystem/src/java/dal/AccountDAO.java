/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;

/**
 *
 * @author Dat
 */
public class AccountDAO {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;

    public void CloseConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (ps != null) {
            rs.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public Account checkMail(String mail) {
        Account a = null;
        try {
            String sql = "SELECT * FROM Account WHERE email=?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, mail);
            rs = ps.executeQuery();
            if (rs.next()) {
                a = new Account(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getString(9),
                        rs.getInt(10),
                        rs.getString(11));

            }
            CloseConnection();
        } catch (Exception ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return a;
    }

    public Account checkUsernameAndPass(String username, String password) {
        try {
            String sql = "SELECT * FROM Account WHERE username = ? AND password = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            rs = ps.executeQuery();
            if (rs.next()) {
                Account a = new Account(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getString(9),
                        rs.getInt(10),
                        rs.getString(11), rs.getInt(12));
                return a;
            }
        } catch (Exception ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void insertAccount(Account a) {
        try {
            String sql = "INSERT INTO [dbo].[Account]\n"
                    + "           ([username]\n"
                    + "           ,[password]\n"
                    + "           ,[displayname]\n"
                    + "           ,[gender]\n"
                    + "           ,[email]\n"
                    + "           ,[phone]\n"
                    + "           ,[address]\n"
                    + "           ,[role]\n"
                    + "           ,[imageUrl]\n"
                    + "           ,[status])\n"
                    + "     VALUES\n"
                    + "           (?,?,?,?,?,?,?,?,?,?)";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, a.getUsername());
            ps.setString(2, a.getPassword());
            ps.setString(3, a.getDisplayname());
            ps.setInt(4, a.getGender());
            ps.setString(5, a.getEmail());
            ps.setString(6, a.getPhone());
            ps.setString(7, a.getAddress());
            ps.setInt(8, a.getRole());
            ps.setString(9, a.getImageUrl());
            ps.setString(10, a.getStatus());
            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public ArrayList<Account> getAllAccounts() {
        ArrayList<Account> list = new ArrayList<>();
        try {
            String sql = "select * from Account";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Account a = new Account(rs.getString(1), rs.getString(2),
                        rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getInt(8),
                        rs.getString(9), rs.getInt(10), rs.getString(11));
                list.add(a);
            }
        } catch (Exception ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public void updateProfile(String username, String displayName, int gender, String email, String phone, String address, String imageUrl) {
        try {
            String sql = "UPDATE [dbo].[Account]\n"
                    + " SET    [displayname] = ?\n"
                    + "      ,[gender] = ?\n"
                    + "      ,[email] = ?\n"
                    + "      ,[phone] = ?\n"
                    + "      ,[address] = ?\n"
                    + "      ,[imageUrl] = ?\n"
                    + " WHERE username = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, displayName);
            ps.setInt(2, gender);
            ps.setString(3, email);
            ps.setString(4, phone);
            ps.setString(5, address);
            ps.setString(6, imageUrl);
            ps.setString(7, username);
            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updatePassword(String username, String password) {
        try {
            String sql = "UPDATE [dbo].[Account]\n"
                    + " SET    [password] = ?\n"
                    + " WHERE username = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, password);
            ps.setString(2, username);
            ps.executeUpdate();
            CloseConnection();
        } catch (Exception ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Account getAccountByUsername(String username) {
        try {
            String sql = "SELECT * FROM Account WHERE username = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            if (rs.next()) {
                Account a = new Account(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getString(9),
                        rs.getInt(10),
                        rs.getString(11),
                        rs.getInt(12));
                return a;
            }
        } catch (Exception ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void updateRole(int role, String username) {
        try {
            String sql = "UPDATE [dbo].[Account]\n"
                    + " SET    [role] = ?\n"
                    + " WHERE username = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, role);
            ps.setString(2, username);
            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateStatus(String newStatus, String username) {
        try {
            String sql = "UPDATE [dbo].[Account]\n"
                    + " SET    [status] = ?\n"
                    + " WHERE username = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, newStatus);
            ps.setString(2, username);
            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        ArrayList<Account> list = new AccountDAO().getAllAccounts();
        for (Account account : list) {
            System.out.println();
        }
    }

    public void requestUpdateRole(String pending, String username) {
        try {
            String sql = "UPDATE [dbo].[Account]\n"
                    + " SET    [status] = ?\n"
                    + " WHERE username = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, pending);
            ps.setString(2, username);
            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Account checkUsernameExist(String username) {
        try {
            String sql = "SELECT * FROM Account WHERE username = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            if (rs.next()) {
                Account a = new Account(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getString(9),
                        rs.getInt(10),
                        rs.getString(11));
                return a;
            }
        } catch (Exception ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void requestUpdateRoleAndCpid(String pending, int cpid, String username) {
        try {
            String sql;
            if (cpid == -1) {
                sql = "UPDATE [dbo].[Account]\n"
                        + "   SET [status] = ?\n"
                        + "      ,[cpid] = null\n"
                        + " WHERE username = ?";
            } else {
                sql = "UPDATE [dbo].[Account]\n"
                        + "   SET [status] = ?\n"
                        + "      ,[cpid] = " + cpid + "\n"
                        + " WHERE username = ?";
            }
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, pending);
            ps.setString(2, username);
            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Account checkUserExist(String username) {
        try {
            String sql = "SELECT [username],\n"
                    + "	[displayname]\n"
                    + "  FROM [Account] where [username] = ? and role != 1";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            if (rs.next()) {
                Account a = new Account(rs.getString(1),
                        rs.getString(2));
                return a;
            }
        } catch (Exception ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Account> getAllAccountsOfRecruiter() {
        ArrayList<Account> list = new ArrayList<>();
        try {
            String sql = "SELECT [username]\n"
                    + "      ,[password]\n"
                    + "      ,[displayname]\n"
                    + "      ,[gender]\n"
                    + "      ,[email]\n"
                    + "      ,[phone]\n"
                    + "      ,[address]\n"
                    + "      ,[role]\n"
                    + "      ,[imageUrl]\n"
                    + "      ,[haveCV]\n"
                    + "      ,[status]\n"
                    + "      ,[cpid]\n"
                    + "  FROM [dbo].[Account] where role= 2";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {                
                Account a = new Account(rs.getString(1), rs.getString(2), 
                        rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), 
                        rs.getInt(8), rs.getString(9), rs.getInt(10), rs.getString(11),rs.getInt(12));
                list.add(a);
            }
        } catch (Exception ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
