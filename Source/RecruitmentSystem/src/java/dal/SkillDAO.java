/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Experience;
import model.Skill;

/**
 *
 * @author huyho
 */
public class SkillDAO {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;

    public ArrayList<Skill> getAllSkillByUsername(String username) {
        ArrayList<Skill> list = new ArrayList<>();
        try {

            String sql = "SELECT [id]\n"
                    + "      ,[skill]\n"
                    + "      ,[level]\n"
                    + "      ,[username]\n"
                    + "  FROM [dbo].[Skill] where username = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                Skill s = new Skill(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4));
                list.add(s);
            }
        } catch (Exception ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public void addMoreSkill(String skill, int level, String username) {
        try {
            String sql = "INSERT INTO [dbo].[Skill]\n"
                    + "           ([skill]\n"
                    + "           ,[level]\n"
                    + "           ,[username])\n"
                    + "     VALUES\n"
                    + "           (?,?,?)";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, skill);
            ps.setInt(2, level);
            ps.setString(3, username);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteSkill(int id) {
        try {
            String sql = "DELETE FROM [dbo].[Skill]\n"
                    + "      WHERE id = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(ExperienceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
