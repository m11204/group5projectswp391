/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.ContactInformation;

/**
 *
 * @author Dat
 */
public class ContactInformationDAO {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;

    public ContactInformation insertContactInformation(String name, String type, String email, String phone, String content, String status) {
        ContactInformation ci = new ContactInformation(name, type, email, phone, content, status);
        try {
            String sql = "INSERT INTO [ContactInformation]\n"
                    + "           ([name]\n"
                    + "           ,[type]\n"
                    + "           ,[email]\n"
                    + "           ,[phone]\n"
                    + "           ,[content]\n"
                    + "           ,[status])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            
            ps.setString(1, ci.getName());
            ps.setString(2, ci.getType());
            ps.setString(3, ci.getEmail());
            ps.setString(4, ci.getPhone());
            ps.setString(5, ci.getContent());
            ps.setString(6, "Pending");
            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(ContactInformationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ci;
    }
}
