/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Message;

/**
 *
 * @author Dat
 */
public class MessageDAO {

    Connection conn;
    PreparedStatement ps;
    ResultSet rs;

    public void CloseConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (ps != null) {
            rs.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public ArrayList<Message> getMessageOf2People(String sendusername, String receiveusername) {
        ArrayList<Message> mlist = new ArrayList<>();

        try {
            String sql = "SELECT [id]\n"
                    + "      ,[content]\n"
                    + "      ,[from]\n"
                    + "      ,[to]\n"
                    + "      ,[time]\n"
                    + "  FROM [Message]\n"
                    + "  WHERE [from] = ? and [to] = ? or [from] = ? and [to] = ? and content != 'null' ";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, sendusername);
            ps.setString(2, receiveusername);
            ps.setString(3, receiveusername);
            ps.setString(4, sendusername);
            rs = ps.executeQuery();
            while (rs.next()) {
                Message m = new Message(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4));
                mlist.add(m);
            }
            CloseConnection();
            return mlist;
        } catch (Exception ex) {
            Logger.getLogger(MessageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Account> getAccountsByRUsernameWithPagging(String rusername, int page, int PAGE_SIZE) {
        ArrayList<Account> acclist = new ArrayList<>();
        try {
            String sql = "SELECT [username], [displayname]\n"
                    + "FROM [Account] INNER JOIN (SELECT [from], MAX([time]) as [time]\n"
                    + "	FROM [Message]\n"
                    + "	where [to] = ?\n"
                    + "	GROUP BY [from]) t ON [Account].[username] = t.[from]\n"
                    + "	ORDER BY t.time DESC"
                    + " offset (?-1)*? row fetch next ? rows only";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, rusername);
            ps.setInt(2, page);
            ps.setInt(3, PAGE_SIZE);
            ps.setInt(4, PAGE_SIZE);
            rs = ps.executeQuery();
            while (rs.next()) {
                Account acc = new Account(rs.getString(1), rs.getString(2));
                acclist.add(acc);
            }
            CloseConnection();
            return acclist;
        } catch (Exception ex) {
            Logger.getLogger(Message.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int getTotalAccountsByRUsername(String rusername) {
        int count = 0;
        try {
            String sql = "SELECT [username], [displayname]\n"
                    + "FROM [Account] INNER JOIN (SELECT [from], MAX([time]) as [time]\n"
                    + "	FROM [Message]\n"
                    + "	where [to] = ?\n"
                    + "	GROUP BY [from]) t ON [Account].[username] = t.[from]\n"
                    + "	ORDER BY t.time DESC";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, rusername);
            rs = ps.executeQuery();
            while (rs.next()) {
                count++;
            }
        } catch (Exception ex) {
            Logger.getLogger(Message.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public void addNewMessage(String rusername, String chooseUser, String sendMessage) {
        try {
            String sql = "INSERT INTO [dbo].[Message]\n"
                    + "           ([content]\n"
                    + "           ,[from]\n"
                    + "           ,[to])\n"
                    + "     VALUES\n"
                    + "           (?,?,?)";
            conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, sendMessage);
            ps.setString(2, rusername);
            ps.setString(3, chooseUser);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(MessageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean checkTheySendMessOrNot(String rusername, String chooseUser) {
        try {
            String sql = "select * from Message where [from] = ? and [to] = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, chooseUser);
            ps.setString(2, rusername);
            rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }

        } catch (Exception ex) {
            Logger.getLogger(MessageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

}
