/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author Dat
 */
public class Job {

    private int id;
    private String name;
    private int cpid;
    private String description;
    private String imageUrl;
    private double salary;
    private boolean isHot;
    private boolean isFulltime;
    private boolean isRemote;
    private String province;
    private String district;
    private int quantity;
    private Date expirationDate;
    private int cid;
    private String detail;
    private String categoryName;

    public Job() {
    }

    public Job(int id, String name, int cpid, String description, String imageUrl, double salary, boolean isHot, boolean isFulltime, boolean isRemote, String province, String district, int quantity, Date expirationDate, int cid, String detail) {
        this.id = id;
        this.name = name;
        this.cpid = cpid;
        this.description = description;
        this.imageUrl = imageUrl;
        this.salary = salary;
        this.isHot = isHot;
        this.isFulltime = isFulltime;
        this.isRemote = isRemote;
        this.province = province;
        this.district = district;
        this.quantity = quantity;
        this.expirationDate = expirationDate;
        this.cid = cid;
        this.detail = detail;
    }

    public Job(int id, String name, int cpid, String description, String imageUrl, double salary, boolean isHot, boolean isFulltime, boolean isRemote, String province, String district, int quantity, Date expirationDate, int cid, String detail, String categoryName) {
        this.id = id;
        this.name = name;
        this.cpid = cpid;
        this.description = description;
        this.imageUrl = imageUrl;
        this.salary = salary;
        this.isHot = isHot;
        this.isFulltime = isFulltime;
        this.isRemote = isRemote;
        this.province = province;
        this.district = district;
        this.quantity = quantity;
        this.expirationDate = expirationDate;
        this.cid = cid;
        this.detail = detail;
        this.categoryName = categoryName;
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCpid() {
        return cpid;
    }

    public void setCpid(int cpid) {
        this.cpid = cpid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public boolean isIsHot() {
        return isHot;
    }

    public void setIsHot(boolean isHot) {
        this.isHot = isHot;
    }

    public boolean isIsFulltime() {
        return isFulltime;
    }

    public void setIsFulltime(boolean isFulltime) {
        this.isFulltime = isFulltime;
    }

    public boolean isIsRemote() {
        return isRemote;
    }

    public void setIsRemote(boolean isRemote) {
        this.isRemote = isRemote;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    
}
