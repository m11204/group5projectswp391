/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author huyho
 */
public class Request {
    private String username;
    private int cpid;
    private int jId;
    private String status;
    private Job job;
    private Company company;
    private Account account;
    public Request() {
    }

    public Request(String username, int cpid, int jId, String status) {
        this.username = username;
        this.cpid = cpid;
        this.jId = jId;
        this.status = status;
    }

    public Request(String username, int cpid, int jId, String status, Job job, Company company, Account account) {
        this.username = username;
        this.cpid = cpid;
        this.jId = jId;
        this.status = status;
        this.job = job;
        this.company = company;
        this.account = account;
    }

    

    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getCpid() {
        return cpid;
    }

    public void setCpid(int cpid) {
        this.cpid = cpid;
    }

    public int getjId() {
        return jId;
    }

    public void setjId(int jId) {
        this.jId = jId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
    
    
    
}
