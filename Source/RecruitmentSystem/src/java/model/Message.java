/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author Dat
 */
public class Message {
    private int id;
    private String content;
    private String from;
    private String to;
    private Timestamp time;

    public Message() {
    }
    
    public Message(int id, String content, String from, String to) {
        this.id = id;
        this.content = content;
        this.from = from;
        this.to = to;
    }

    public Message(int id, String content, String from, String to, Timestamp time) {
        this.id = id;
        this.content = content;
        this.from = from ;
        this.to = to;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }


   

    
}
