/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author huyho
 */
public class Company {
    private int id;
    private String name;
    private String address;
    private String description;
    private String phone;
    private String imageURL;
    private String status;
    private int personalSize;
    public Company() {
    }

    public Company(int id, String name, String address, String description, String phone, String imageURL,String status) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.description = description;
        this.phone = phone;
        this.imageURL = imageURL;
        this.status = status;
    }

    public Company(int id, String name, String address, String description, String phone, String imageURL, String status, int personalSize) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.description = description;
        this.phone = phone;
        this.imageURL = imageURL;
        this.status = status;
        this.personalSize = personalSize;
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPersonalSize() {
        return personalSize;
    }

    public void setPersonalSize(int personalSize) {
        this.personalSize = personalSize;
    }
    
    
    
    
}
