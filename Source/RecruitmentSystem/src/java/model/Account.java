/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Dat
 */
public class Account {
    private String username;
    private String password;
    private String displayname;
    private int gender;
    private String email;
    private String phone;
    private String address;
    private int role;
    private String imageUrl;
    private int haveCV ;
    private String status;
    private int cpid;
    public Account() {
    }

    public Account(String username, String password, String displayname, int gender, String email, String phone, String address, int role, String imageUrl,int haveCV) {
        this.username = username;
        this.password = password;
        this.displayname = displayname;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.role = role;
        this.imageUrl = imageUrl;
        this.haveCV = haveCV ;
    }

    public Account(String username, String password, String displayname, int gender, String email, String phone, String address, int role, String imageUrl, int haveCV, String status) {
        this.username = username;
        this.password = password;
        this.displayname = displayname;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.role = role;
        this.imageUrl = imageUrl;
        this.haveCV = haveCV;
        this.status = status;
    }
    
    

    public Account(String username, String password, String displayname, int gender, String email, String phone, String address, int role, String imageUrl, int haveCV, String status,
            int cpid) {
        this.username = username;
        this.password = password;
        this.displayname = displayname;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.role = role;
        this.imageUrl = imageUrl;
        this.haveCV = haveCV;
        this.status = status;
        this.cpid = cpid;
    }

    public Account(String username, String displayname) {
        this.username = username;
        this.displayname = displayname;
    }
    

    

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getHaveCV() {
        return haveCV;
    }

    public void setHaveCV(int haveCV) {
        this.haveCV = haveCV;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCpid() {
        return cpid;
    }

    public void setCpid(int cpid) {
        this.cpid = cpid;
    }
    
    
}
