<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <%@ include file="meta.jsp" %>
        <title>Recruitment System</title>
        <%@ include file="header.jsp" %>
    </head>

    <body class="bg-image d-flex flex-column min-vh-100"
          style="background-color:#fff0f0;">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-image: url('images/navbar-background.jpg');">
            <div class="container px-lg-5">
                <a class="navbar-brand" href="home">Home</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <form action="search" method="GET" class="d-flex w-50">
                        <input type="hidden" value="${cid}" name="cid" />
                        <input type="hidden" value="${page}" name="page" />
                        <input type="hidden" value="${salary}" name="salary" />
                        <input class="form-control me-2" type="search" placeholder="What's on your mind?" value="${txtSearch}" name="txt">
                        <button class="btn btn-outline-danger" type="submit">Search</button>
                    </form>
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="#">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact</a>
                        </li>

                        <c:choose>
                            <c:when test="${sessionScope.account==null}">
                                <li class="nav-item">
                                    <a class="nav-link" href="login">Login</a>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <c:if test="${sessionScope.account.role != 1}">
                                    <li class="nav-item">
                                        <a class="nav-link" href="messenger">Messenger</a>
                                    </li>
                                </c:if>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        ${sessionScope.account.displayname}
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <li><a class="dropdown-item" href="profile">Profile</a></li>
                                            <c:if test="${sessionScope.account.role == 1}">
                                            <li><a class="dropdown-item" href="dashboard">Dashboard</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.account.role == 2}">
                                            <li><a class="dropdown-item" href="managementOfRecruiment.jsp">Management</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.account.role == 3}">
                                            <li><a class="dropdown-item" href="manage-cv">CV Manager</a></li>
                                            </c:if>
                                        <li><a class="dropdown-item" href="change-pass">Change password</a></li>
                                        <li><hr class="dropdown-divider"></li>
                                        <li><a class="dropdown-item" href="logout">Log out</a></li>
                                    </ul>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </ul>   

                </div>
            </div>
        </nav>
        <!-- /Breadcrumb -->
        <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="150">
                            <div class="mt-3">
                                <h4>${account.displayname}</h4>
                                <p class="text-secondary mb-1">${role}</p>
                                <p class="text-muted font-size-sm">${account.address}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Full Name</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                ${account.displayname}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Gender</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                ${gender}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Email</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                ${account.email}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Phone</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                ${account.phone}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Address</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                ${account.address}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">ImageURL</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                ${account.imageUrl}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Role</h6>
                            </div>
                            <c:if test="${account.role == 3 && account.status eq 'Active'}">
                                <form action="editrole">
                                    <input type="hidden" value="${account.username}" name="username" />
                                    <div class="col-sm-9 text-secondary">
                                        User &nbsp;&nbsp;&nbsp;&nbsp;
                                        <button type="submit">I want become to Recruiter</button>
                                    </div>
                                </form>
                            </c:if>

                            <c:if test="${account.role == 3 && account.status eq 'Pending'}">
                                <form action="profile" method="POST">
                                    <input type="hidden" value="${account.username}" name="username" />
                                    <input type="hidden" value="cancel" name="cancel" />
                                    <div class="col-sm-9 text-secondary">
                                        User &nbsp;&nbsp;&nbsp;&nbsp;
                                        <p>Waiting for approval &nbsp;&nbsp; <button type="submit">Cancel</button></p>
                                    </div>
                                </form>
                            </c:if>
                            <c:if test="${account.role == 2}">
                                <div class="col-sm-9 text-secondary">
                                    Recruiter
                                </div>
                            </c:if>
                            <c:if test="${account.role == 1}">
                                <div class="col-sm-9 text-secondary">
                                    Admin
                                </div>
                            </c:if>
                        </div>
                        <hr>
                        <form action="profile" method="Post">
                            <input type="hidden" name="displayname" value="${account.displayname}" />
                            <input type="hidden" name="gender" value="${account.gender}" />
                            <input type="hidden" name="email" value="${account.email}" />
                            <input type="hidden" name="phone" value="${account.phone}" />
                            <input type="hidden" name="address" value="${account.address}" />
                            <input type="hidden" name="address" value="${account.imageUrl}" />
                            <div class="row">
                                <div class="col-sm-12">
                                    <a class="btn btn-info " href="editprofile?account=${account}">Edit</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="footer.jsp" %>  
</body>
</html>
