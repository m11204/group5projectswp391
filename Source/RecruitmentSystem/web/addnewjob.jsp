<%-- 
    Document   : addnewcat
    Created on : May 21, 2022, 5:20:10 PM
    Author     : huyho
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="css/stylelogin.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div style="width: 400px; margin: auto">
            <form action="add-new-job" method="POST">
                <h1>Create New Job</h1>
                <h4 style="color: green">${success}</h4>
                <div class="mb-3 mt-3">
                    <label for="name" class="form-label">Name</label>
                    <input required="" type="text" class="form-control" value="${name}" id="name" name="name">
                </div>
                <div class="mb-3 mt-3">
                    <label for="description" class="form-label">Short Description</label>
                    <input required="" type="text" class="form-control" id="description" value="${description}" name="description">
                </div>
                <div class="mb-3 mt-3">
                    <label for="imageUrl" class="form-label">ImageURL</label>
                    <input type="text" class="form-control" id="imageUrl" value="${imageUrl}" name="imageUrl">
                </div>
                <div class="mb-3 mt-3">
                    <h3 style="color: red">${error}</h3>
                    <label for="salary" class="form-label">Salary</label>
                    <input required="" type="text" class="form-control" id="salary"  name="salary">
                </div>
                <div class="mb-3 mt-3">
                    <label style="font-weight: 600" class="form-label">isFulltime</label>
                    <input required=""  type="radio"  id="isFulltime" ${isFulltime_raw eq '1'?"checked":""} value="1" name="isFulltime">Yes
                    <input  type="radio"  id="isFulltime" ${isFulltime_raw eq '0'?"checked":""} value="0" name="isFulltime">No

                </div>
                <div class="mb-3 mt-3">
                    <label for="province" class="form-label">Province</label>
                    <input required="" type="text" class="form-control" id="province" value="${province}" name="province">
                </div>
                <div class="mb-3 mt-3">
                    <label for="district" class="form-label">District</label>
                    <input required="" type="text" class="form-control" id="district" value="${district}" name="district">
                </div>
                <div class="mb-3 mt-3">
                    <label for="quantity" class="form-label">Quantity</label>
                    <input required="" type="number" class="form-control" value="${quantity}" id="quantity" name="quantity">
                </div>

                <div class="mb-3 mt-3">
                    <label for="quantity" class="form-label">Category</label>
                    <select name="categoryId">
                        <c:forEach items="${listCategorys}" var="C">
                            <option ${C.id == catId?"selected":""} value="${C.id}">${C.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="mb-3 mt-3">
                    <label for="detail" class="form-label">Detail</label>
                    <input required="" type="tx" class="form-control" id="detail" value="${detail}" name="detail">
                </div>
                <button type="submit" class="btn btn-primary">Add</button>
                <a href="management?type=1" class="btn btn-primary">Cancel</a>
            </form>
        </div>
    </body>
</html>
