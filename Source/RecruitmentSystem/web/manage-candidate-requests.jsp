<%-- 
    Document   : tables
    Created on : May 20, 2022, 12:25:45 AM
    Author     : huyho
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Manager Account - SB Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="css/styleDashboard.css" rel="stylesheet"/>
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Management Requests</h1>
                        <ol class="breadcrumb mb-4">
                            <a href="" style="background-color:green " class="btn text-white"><< Back to home</a>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                <%int count = 1;%>
                                <table id="datatablesSimple">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Job Name</th>
                                            <th>Candidate Username</th>
                                            <th>Candidate mail</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${listRequests}" var="R">
                                            <tr>
                                                <td><%= count++%></td>
                                                <td>${R.job.name}</td>
                                                <td>${R.username}</td>
                                                <td>${R.account.email}</td>
                                                <td>
                                                    <a class="text-decoration-none" style="background-color: gray; padding: 3px 3px;color: white" href="candidate-detail?username=${R.username}">ViewCV</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <c:if test="${R.status eq 'Pending'}">
                                                        <!-- Button trigger modal -->
                                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                                            Approve
                                                        </button>

                                                        <!-- Modal -->
                                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <form action="approve-cancel-request" method="POST">
                                                                    <div class="modal-content">
                                                                        <input type="hidden" name="username" value="${R.account.username}" />
                                                                        <input type="hidden" name="emailUser" value="${R.account.email}" />
                                                                        <input type="hidden" name="cpid" value="${R.cpid}" />
                                                                        <input type="hidden" name="jid" value="${R.jId}" />
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Enter your information to email the Candidate</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <table>
                                                                                <tr>
                                                                                    <td>Your Email</td>
                                                                                    <td><input type="text" name="email" /></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Password Email</td>
                                                                                    <td><input type="password" name="pass" /></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Your message</td>
                                                                                    <td><textarea id="id" name="message" rows="5" cols="50"></textarea></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${R.status eq 'Active'}">
                                                        <a class="text-decoration-none" style="background-color: green; padding: 3px 3px;color: white" href="approve-cancel-request?username=${R.username}&cpid=${R.cpid}&jid=${R.jId}">Cancel</a>
                                                    </c:if>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="js/datatables-simple-demo.js"></script>
    </body>
</html>
