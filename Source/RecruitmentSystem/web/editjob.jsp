<%-- 
    Document   : tables
    Created on : May 20, 2022, 12:25:45 AM
    Author     : huyho
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Manager Account - SB Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="css/styleDashboard.css" rel="stylesheet"/>
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        
         <script type="text/javascript">
            function deleteJob(id){
                if(confirm("are U sure to delete ?")){
                    window.location = "delete-job?id="+id;
                }
            }
        </script>
    </head>
    <body class="sb-nav-fixed">
        <div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Management Jobs</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Tables</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                <h3 style="color: red">${error}</h3> 
                                <form action="edit-job" method="POST">
                                    <table id="datatablesSimple">

                                        <thead>
                                            <tr>
                                                <th>Name job</th>
                                                <th>Category Name</th>
                                                <th>Description</th>
                                                <th>ImageURL</th>
                                                <th>Salary</th>
                                                <th>isFullTime</th>
                                                <th>Province</th>
                                                <th>District</th>
                                                <th>Quantity</th>
                                                <th>Detail</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <c:forEach items="${listJobs}" var="J">
                                                <c:choose>
                                                    <c:when test="${J.id == job.id}">
                                                        <tr>
                                                            <td>
                                                                <input type="hidden" name="id" value="${J.id}" />
                                                                <input type="text" name="name" value="${J.name}" /></td>
                                                            <td>
                                                                <select name="categoryId">
                                                                    <c:forEach items="${listCategorys}" var="C">
                                                                        <option value="${C.id}">${C.name}</option>
                                                                    </c:forEach>
                                                                </select>
                                                            </td>
                                                            <td><input type="text" name="description" value="${J.description}" /></td>
                                                            <td><input type="text" name="imageUrl" value="${J.imageUrl}" /></td>
                                                            <td><input type="text" name="salary" value="${J.salary}" /></td>
                                                                <c:if test="${J.isFulltime}">
                                                                <td><input type="radio" name="isFulltime" value="1" checked=""/>Yes
                                                                    <input type="radio" name="isFulltime" value="0"/>No
                                                                </td>
                                                            </c:if>
                                                            <c:if test="${!J.isFulltime}">
                                                                <td><input type="radio" name="isFulltime" />Yes
                                                                    <input type="radio" name="isFulltime" checked=""/>No
                                                                </td>
                                                            </c:if>
                                                            <td><input type="text" name="province" value="${J.province}" /></td>
                                                            <td><input type="text" name="district" value="${J.district}" /></td>
                                                            <td><input type="number" min="0" max="${maxvalue}" name="quantity" value="${J.quantity}" /></td>
                                                            <td><input type="text" name="detail" value="${J.detail}" /></td>
                                                            <td>
                                                                <div>
                                                                    <button type="submit" value="Save">Save</a></button>
                                                                    <a onclick="deleteJob(${J.id})" href="">Delete</a>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </c:when>
                                                    <c:otherwise>
                                                        <tr>
                                                            <td>${J.name}</td>
                                                            <td>${J.categoryName}</td>
                                                            <td>${J.description}</td>
                                                            <td>${J.imageUrl}</td>
                                                            <td>${J.salary}</td>
                                                            <c:if test="${J.isFulltime == true}">
                                                                <td>Yes</td>
                                                            </c:if>
                                                            <c:if test="${J.isFulltime == false}">
                                                                <td>No</td>
                                                            </c:if>
                                                            <td>${J.province}</td>
                                                            <td>${J.district}</td>
                                                            <td>${J.quantity}</td>
                                                            <td>${J.detail}</td>
                                                            <td><a href="edit-job?id=${J.id}">Edit</a>&nbsp;&nbsp;&nbsp; <a onclick="deleteJob(${J.id})" href="">Delete</a></td></td>
                                                        </tr> 
                                                    </c:otherwise>

                                                </c:choose>

                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="js/datatables-simple-demo.js"></script>
    </body>
</html>
