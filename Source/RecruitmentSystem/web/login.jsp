<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="meta.jsp" %>
        <title>Login</title>
        <%@ include file="header.jsp" %>
    </head>
    <body class="bg-image" 
          style="background-color:#fff0f0;">
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-image: url('images/navbar-background.jpg');">
            <div class="container px-lg-5">
                <a class="navbar-brand" href="home">Home</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <form action="search" method="GET" class="d-flex w-50">
                        <input type="hidden" value="${cid}" name="cid" />
                        <input type="hidden" value="${page}" name="page" />
                        <input type="hidden" value="${salary}" name="salary" />
                        <input class="form-control me-2" type="search" placeholder="What's on your mind?" value="${txtSearch}" name="txt">
                        <button class="btn btn-outline-danger" type="submit">Search</button>
                    </form>
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="#">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="login">Login</a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
        <section class="vh-100">
            <div class="container-fluid h-custom">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-md-9 col-lg-6 col-xl-5">
                        <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
                             class="img-fluid" alt="Sample image">
                    </div>
                    <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
                        <form action="login" method="POST">

                            <!-- user input -->
                            <h4 style="color: green">${success}</h4>
                            <h4 style="color: red">${blocked}</h4>
                            <div class="form-outline mb-4">
                                <label class="form-label" for="user">User name</label>
                                <input type="text" id="user" class="form-control form-control-lg"
                                       placeholder="User name" name="username"/>

                            </div>

                            <!-- Password input -->
                            <div class="form-outline mb-3">
                                <label class="form-label" for="password">Password</label>
                                <input type="password" id="password" class="form-control form-control-lg"
                                       placeholder="Enter password" name="password"/>

                            </div>

                            <div class="d-flex justify-content-between align-items-center">
                                <!-- Checkbox -->
                                <div class="form-check mb-0">
                                    <input class="form-check-input me-2" type="checkbox" value="true" id="form2Example3" name="remember"/>
                                    <label class="form-check-label" for="form2Example3">
                                        Remember me
                                    </label>
                                </div>
                                <a href="forgetpassword.jsp" class="text-body">Forgot password?</a>
                            </div>

                            <div>
                                <h5 class="link-danger">
                                    ${error}
                                </h5>
                            </div>

                            <div class="text-center text-lg-start mt-4 pt-2">
                                <button type="submit" class="btn btn-outline-danger btn-lg"
                                        style="padding-left: 2.5rem; padding-right: 2.5rem;">Login</button>
                                <p class="small fw-bold mt-2 pt-1 mb-0">Don't have an account? <a href="register"
                                                                                                  class="link-danger">Register</a></p>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </section>
        <%@ include file="footer.jsp" %> 
    </body>
</html>
