<%-- 
    Document   : profile
    Created on : May 19, 2022, 3:12:15 PM
    Author     : huyho
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/profile.css" rel="stylesheet"/>
        <link href="css/styles.css" rel="stylesheet"/>
    </head>
    <body>
        <div class="container">
            <div class="main-body">

                <!-- Breadcrumb -->
                <nav aria-label="breadcrumb" class="main-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">User Profile</li>
                    </ol>
                </nav>
                <!-- /Breadcrumb -->
                <div class="row gutters-sm">
                    <div style="margin-bottom: 100px" class="text-center">

                        <form action="editrole" method="POST">
                            <input type="hidden" value="1" name="option" />
                            <h1 class="mb-0">Choose Company</h1>
                            <select onchange="this.form.submit()" name="companyId">
                                <option>Company</option>
                                <c:forEach items="${listCompanys}" var="C">
                                    <option value="${C.id}">${C.name}</option>
                                </c:forEach>
                            </select>
                        </form>
                    </div>
                    <div class="col-md-12">
                        <h3>Other</h3>
                        <div class="card mb-3">
                            <div class="card-body">
                                <form action="editrole" method="POST">
                                    <input type="hidden" value="2" name="option" /> 
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Company Name</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input required="" type="text" name="company-other" />
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">address</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input required="" type="text" name="address" />
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Description</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input required="" type="text" name="description"/>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Phone</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input required="" type="number" name="phone"/>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">ImageUrl</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input type="text" name="imageUrl"/>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <input class="btn btn-outline-success"  type="submit" value="Save"/>
                                        </div>
                                    </div>
                                    <h3 style="color: red">${error}</h3>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
