<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <%@ include file="meta.jsp" %>
        <title>Recruitment System</title>
        <%@ include file="header.jsp" %>
        <link href="css/candidate.css" rel="stylesheet" type="text/css"/>
    </head>

    <body class="bg-image" 
          style="background-color:#fff0f0;">
        <!-- Navigation-->
        <%@include file="navbar.jsp" %>
        <!-- Header-->
        <header class="py-5" >
            <div class="container px-lg-5" >
                <div class="p-4 p-lg-5 bg-light rounded-3 text-center" style="background-image: url('images/navbar-background.jpg');">
                    <div class="m-4 m-lg-5">
                        <h1 class="display-5 fw-bold" >Welcome to Recruitment System</h1>
                        <p class="fs-4">Bootstrap utility classes are used to create this jumbotron since the old component
                            has been removed from the framework. Why create custom CSS when you can use utilities?</p>
                        <a class="btn btn-outline-danger btn-lg" href="#!">Call to action</a>
                    </div>
                </div>
            </div>
        </header>
        <!--home screen for Recuiment--> 

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" integrity="sha256-2XFplPlrFClt0bIdPgpz8H7ojnk10H69xRqd9+uTShA=" crossorigin="anonymous" />

        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-9">
                    <h3>Candidate related to work</h3>
                    <div class="row">
                        <c:forEach items="${listAccountsCandidate}" var="A">
                            <div class="col-sm-6 col-lg-4 mb-4">
                                <div class="candidate-list candidate-grid">
                                    <div class="candidate-list-image">
                                        <img class="img-fluid" src="${A.imageUrl}" alt="">
                                    </div>
                                    <div class="candidate-list-details">
                                        <div class="candidate-list-info">
                                            <div class="candidate-list-title">
                                                <h5><a href="candidate-detail?username=${A.username}">${A.displayname}</a></h5>
                                            </div>
                                            <div class="candidate-list-option">
                                                <ul class="list-unstyled">
                                                    <c:forEach items="${userAndJob}" var="U">
                                                        <c:if test="${U.key eq A.username}">
                                                            <li><i class="fas fa-filter pr-1"></i>${U.value}</li>
                                                            </c:if>
                                                        </c:forEach>
                                                    <li><i class="fas fa-map-marker-alt pr-1"></i>${A.address}</li>

                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-list-favourite-time">
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </c:forEach>
                    </div>

                    <div class="row">
                        <div class="col-12 text-center mt-4 mt-sm-5">
                            <ul class="pagination justify-content-center mb-0">
                                <li class="page-item ${page == 1?"disabled":""}">
                                    <a class="page-link" href="home-recruiter?page=${page - 1}" tabindex="-1" aria-disabled="true">Previous</a>
                                </li>
                                <c:forEach begin="1" end="${totalPage}" var="i">
                                    <li class="page-item ${i == page? "active":""}"><a class="page-link" href="home-recruiter?page=${i}">${i}</a></li>
                                    </c:forEach>
                                <li class="page-item  ${page == totalPage?"disabled":""}">
                                    <a class="page-link" href="home-recruiter?page=${page + 1}">Next</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file="footer.jsp" %>  
    </body>

</html>
