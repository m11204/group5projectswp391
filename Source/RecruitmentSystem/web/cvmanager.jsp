<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <%@ include file="meta.jsp" %>
        <title>Recruitment System</title>
        <%@ include file="header.jsp" %>
    </head>
    <body class="bg-image" 
          style="background-color:#fff0f0;">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-image: url('images/navbar-background.jpg');">
            <div class="container px-lg-5">
                <a class="navbar-brand" href="home">Home</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <form action="search" method="GET" class="d-flex w-50">
                        <input type="hidden" value="${cid}" name="cid" />
                        <input type="hidden" value="${page}" name="page" />
                        <input type="hidden" value="${salary}" name="salary" />
                        <input class="form-control me-2" type="search" placeholder="What's on your mind?" value="${txtSearch}" name="txt">
                        <button class="btn btn-outline-danger" type="submit">Search</button>
                    </form>
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="#">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact</a>
                        </li>

                        <c:choose>
                            <c:when test="${sessionScope.account==null}">
                                <li class="nav-item">
                                    <a class="nav-link" href="login">Login</a>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <c:if test="${sessionScope.account.role != 1}">
                                    <li class="nav-item">
                                        <a class="nav-link" href="messenger">Messenger</a>
                                    </li>
                                </c:if>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        ${sessionScope.account.displayname}
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <li><a class="dropdown-item" href="profile">Profile</a></li>
                                            <c:if test="${sessionScope.account.role == 1}">
                                            <li><a class="dropdown-item" href="dashboard">Dashboard</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.account.role == 2}">
                                            <li><a class="dropdown-item" href="managementOfRecruiment.jsp">Management</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.account.role == 3}">
                                            <li><a class="dropdown-item" href="manage-cv">CV Manager</a></li>
                                            </c:if>
                                        <li><a class="dropdown-item" href="change-pass">Change password</a></li>
                                        <li><hr class="dropdown-divider"></li>
                                        <li><a class="dropdown-item" href="logout">Log out</a></li>
                                    </ul>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </ul>

                </div>
            </div>
        </nav>
        <header class="py-5" >
            <div class="container px-lg-5" >
                <div class="p-4 p-lg-5 bg-light rounded-3 text-center" style="background-image: url('images/navbar-background.jpg');">
                    <div class="m-4 m-lg-5">
                        <h1 class="display-5 fw-bold" >Welcome to CV/Resume Manager</h1>
                        <p class="fs-4">Bootstrap utility classes are used to create this jumbotron since the old component
                            has been removed from the framework. Why create custom CSS when you can use utilities?</p>
                        <a class="btn btn-outline-danger btn-lg" href="#!">Call to action</a>
                    </div>
                </div>
            </div>
        </header>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />


        <div class="container">
            <div class="mt-5">
                <div class="d-style btn btn-brc-tp border-2 bgc-white btn-outline-blue btn-h-outline-blue btn-a-outline-blue w-100 my-2 py-3 shadow-sm">
                    <!-- Basic Plan -->

                    <div class="row align-items-center">
                        <div class="col-12 col-md-4">
                            <h4 class="pt-3 text-170 text-600 text-primary-d1 letter-spacing">
                                Create CV/Resume
                            </h4>
                        </div>

                        <ul class="list-unstyled mb-0 col-12 col-md-4 text-dark-l1 text-90 text-left my-4 my-md-0">
                            <li>
                                <i class="fa fa-check text-success-m2 text-110 mr-2 mt-1"></i>

                                <c:choose>
                                    <c:when test="${about.status == 1}">
                                        <form action="manage-cv" method="POST">
                                            <input type="hidden" name="type" value="off" />
                                            <span>
                                                <button class="btn btn-warning">Turn off job search status</button>
                                            </span>
                                        </form>
                                    </c:when>
                                    <c:otherwise>
                                        <form action="manage-cv" method="POST">
                                             <input type="hidden" name="type" value="on" />
                                            <span>
                                                <button class="btn bg-success">Turn on job search status</button>
                                            </span>
                                        </form>
                                    </c:otherwise>
                                </c:choose>

                            </li>
                        </ul>

                        <div class="col-12 col-md-4 text-center">
                            <a href="add-cv" class="f-n-hover btn btn-outline-danger btn-raised px-4 py-25 w-75 text-600">Create</a>
                        </div>
                    </div>

                </div>



                <div class="d-style bgc-white btn btn-brc-tp btn-outline-green btn-h-outline-green btn-a-outline-green w-100 my-2 py-3 shadow-sm border-2">
                    <!-- Pro Plan -->
                    <div class="row align-items-center">
                        <div class="col-12 col-md-4">
                            <h4 class="pt-3 text-170 text-600 text-green-d1 letter-spacing">
                                Upload CV/Resume
                            </h4>
                        </div>

                        <ul class="list-unstyled mb-0 col-12 col-md-4 text-dark-l1 text-90 text-left my-4 my-md-0">
                            <li>
                                <i class="fa fa-check text-success-m2 text-110 mr-2 mt-1"></i>
                                <span>
                                    <span class="text-110">Fast</span>
                                </span>
                            </li>

                            <li class="mt-25">
                                <i class="fa fa-check text-success-m2 text-110 mr-2 mt-1"></i>
                                <span class="text-110">
                                    PDF accepted
                                </span>
                            </li>

                            <li class="mt-25">
                                <i class="fa fa-check text-success-m2 text-110 mr-2 mt-1"></i>
                                <span class="text-110">
                                    Security
                                </span>
                            </li>
                        </ul>

                        <div class="col-12 col-md-4 text-center">
                            <a href="#" class="f-n-hover btn btn-outline-danger btn-raised px-4 py-25 w-75 text-600">Upload</a>
                        </div>
                    </div>

                </div>



                <div class="d-style btn btn-brc-tp border-2 bgc-white btn-outline-purple btn-h-outline-purple btn-a-outline-purple w-100 my-2 py-3 shadow-sm">
                    <!-- Premium Plan -->
                    <div class="row align-items-center">
                        <div class="col-12 col-md-4">
                            <h4 class="pt-3 text-170 text-600 text-purple-d1 letter-spacing">
                                CV/Resume Settings
                            </h4>
                        </div>

                        <ul class="list-unstyled mb-0 col-12 col-md-4 text-dark-l1 text-90 text-left my-4 my-md-0">
                            <li>
                                <i class="fa fa-check text-success-m2 text-110 mr-2 mt-1"></i>
                                <span>
                                    <span class="text-110">CV manage</span>
                                </span>
                            </li>

                            <li class="mt-25">
                                <i class="fa fa-check text-success-m2 text-110 mr-2 mt-1"></i>
                                <span class="text-110">
                                    CV visible
                                </span>
                            </li>

                            <li class="mt-25">
                                <i class="fa fa-check text-success-m2 text-110 mr-2 mt-1"></i>
                                <span class="text-110">
                                    CV plan
                                </span>
                            </li>
                        </ul>

                        <div class="col-12 col-md-4 text-center">
                            <a href="#" class="f-n-hover btn btn-outline-danger btn-raised px-4 py-25 w-75 text-600">Setting</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>  
        <br>
        <%@ include file="footer.jsp" %>  
    </body>

</html>
