<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <%@ include file="meta.jsp" %>
        <title>Recruitment System</title>
        <%@ include file="header.jsp" %>
        <link href="css/candidate.css" rel="stylesheet" type="text/css"/>
    </head>

    <body class="bg-image" 
          style="background-color:#fff0f0;">
        <!-- Navigation-->
        <%@include file="navbar.jsp" %>
        <!-- Header-->
        <header class="py-5" >
            <div class="container px-lg-5" >
                <div class="p-4 p-lg-5 bg-light rounded-3 text-center" style="background-image: url('images/navbar-background.jpg');">
                    <div class="m-4 m-lg-5">
                        <h1 class="display-5 fw-bold" >Welcome to Recruitment System</h1>
                        <p class="fs-4">Bootstrap utility classes are used to create this jumbotron since the old component
                            has been removed from the framework. Why create custom CSS when you can use utilities?</p>
                        <a class="btn btn-outline-danger btn-lg" href="#!">Call to action</a>
                    </div>
                </div>
            </div>
        </header>
        <!-- Section-->
            <section class="py-5">
                <div class="container px-4 px-lg-5 mt-5">
                    <div class="row">
                        <div class="col-md-4">
                            <form action="search" method="GET">      
                                <h3>List Categories</h3> 
                                <select onchange="this.form.submit()" name="cid">
                                    <option value="-1">All Categories</option>
                                    <c:forEach items="${clist}" var="C">
                                        <option ${C.id == cid?"selected":""} value="${C.id}">${C.name}</option>
                                    </c:forEach>
                                </select>

                                <div style="margin-top: 10px">
                                    <h3>Salary</h3> 
                                    <input ${salary == -1?"checked":""} onclick="this.form.submit()" value="-1" type="radio" name="salary" />All<br/>
                                    <input ${salary == 1?"checked":""} onclick="this.form.submit()" type="radio" name="salary" value="1" />less than 1000<br/>
                                    <input ${salary == 2?"checked":""} onclick="this.form.submit()" type="radio" name="salary" value="2" />between 1000 and 2000<br/>
                                    <input ${salary == 3?"checked":""} onclick="this.form.submit()" type="radio" name="salary" value="3" />more than 2000<br/>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-8">
                            <h3>List Jobs</h3>
                            <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                                <c:forEach items="${jlist}" var="J"> 
                                    <div class="col mb-5">
                                        <div class="card h-100">
                                            <!-- Product image-->
                                            <img class="card-img-top" src="https://anhdep.tv/attachments/a739c4cf6d089e96809e1bd84bc41ac4-jpeg.5858/"
                                                 alt="..." />
                                            <!-- Product details-->
                                            <div class="card-body p-4">
                                                <div class="text-center">
                                                    <!-- Product name-->
                                                    <h5 class="fw-bolder">${J.name}</h5>
                                                    <!-- Product reviews-->
                                                    <div class="d-flex justify-content-center small text-warning mb-2">
                                                        <div class="bi-star-fill"></div>
                                                        <div class="bi-star-fill"></div>
                                                        <div class="bi-star-fill"></div>
                                                        <div class="bi-star-fill"></div>
                                                        <div class="bi-star-fill"></div>
                                                    </div>
                                                    <!-- Product price-->
                                                    <h6>wage  ${J.salary}</h6>
                                                    <p>${J.description}</p>
                                                    <c:if test="${J.isHot}">
                                                        <span style="background-color: red; color: white">Hot</span><br/>
                                                    </c:if>

                                                    <c:choose>
                                                        <c:when test="${J.isFulltime}">
                                                            <span style="background-color: green; color: white">Full Time</span>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <span style="background-color: green; color: white">Part Time</span>
                                                        </c:otherwise>
                                                    </c:choose>

                                                    <c:if test="${J.isRemote}">
                                                        <span style="background-color: #0dcaf0; color: white">Remote</span><br/>
                                                    </c:if>
                                                </div>
                                            </div>
                                            <!-- Product actions-->
                                            <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                                <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="#">View Job details</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>

                            </div>
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-center">
                                    <li class="page-item ${page == 1?"disabled":""}">
                                        <a class="page-link" href="home?page=${page - 1}" tabindex="-1" aria-disabled="true">Previous</a>
                                    </li>
                                    <c:forEach begin="1" end="${totalPage}" var="i">
                                        <li class="page-item ${i == page? "active":""}"><a class="page-link" href="home?page=${i}&salary=${salary}">${i}</a></li>
                                        </c:forEach>
                                    <li class="page-item  ${page == totalPage?"disabled":""}">
                                        <a class="page-link" href="home?page=${page + 1}">Next</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </section>
        <%@ include file="footer.jsp" %>  
    </body>

</html>
