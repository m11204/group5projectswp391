<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<footer class="bg-dark mt-auto" style="background-image: url('images/navbar-background.jpg');">
    <div
        class="d-flex flex-column flex-md-row text-center text-md-start justify-content-between py-4 px-4 px-xl-5">
        <div class="text-black mb-3 mb-md-0">
            Recruitment System
            Copyright © 2022. All rights reserved.
            <br>
            Address: Street X, Y Province, Viet Nam
            <br>
            Phone: 0123456789
        </div>
        <div> 
            <form action="success" method="POST">
                Contact & Support:<br>
                <input type="text" placeholder="Full name(*)" name="name">
                <input type="text" placeholder="Contact type(*)" name="type">
                <br>
                <input type="email" placeholder="Email(*)" name="email">
                <input type="text" placeholder="Phone(*)" name="phone">
                <br>
                <input type="text" placeholder="Content" name="content">
                <input type="submit" value="Send">
            </form>
        </div>
    </div>
</footer>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="js/scripts.js"></script>