<%-- 
    Document   : register
    Created on : May 18, 2022, 12:39:58 AM
    Author     : Dat
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register</title>
        <link href="css/styles.css" rel="stylesheet"/>
    </head>
    <body>
        <section class="vh-100" style="background-color: #eee;">
            <div class="container h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-lg-12 col-xl-11">
                        <div class="card text-black" style="border-radius: 25px;">
                            <div class="card-body p-md-5">
                                <div class="row justify-content-center">
                                    <div class="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">

                                        <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Register</p>
                                        <h3 class="text-danger">${failed}</h3>
                                       
                                        <form class="mx-1 mx-md-4" action="register" method="POST">
                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="username">User name</label>
                                                    <input type="text" id="username" class="form-control" name="username" required="" value="${username}"/>
                                                    
                                                </div>
                                            </div>                                        

                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-lock fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="password">Password</label>
                                                    <input type="password" id="password" class="form-control" name="password" required="" value="${password}"/>
                                                    
                                                </div>
                                            </div>

                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-key fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="repassword">Repeat your password</label>
                                                    <input type="password" id="repassword" class="form-control" name="repassword" required=""/>
                                                    <h6 class="link-danger">${error}</h6>
                                                </div>
                                            </div>
                                            
                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="first">First name</label>
                                                    <input type="text" id="first" class="form-control" name="first" required="" value="${first}"/>
                                                    
                                                </div>
                                            </div> 
                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="last">Last name</label>
                                                    <input type="text" id="last" class="form-control" name="last" required="" value="${last}"/>
                                                </div>
                                            </div> 

                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="gender">Gender: </label>
                                                    <input type="radio" id="gender" name="gender" value="1" ${gender == 1?"checked":""}/>&nbsp;Male &nbsp;&nbsp;
                                                    <input type="radio" id="gender" name="gender" value="0" ${gender == 0?"checked":""}/>&nbsp;FeMale &nbsp;&nbsp;  
                                                    <input type="radio" id="gender" name="gender" value="-1" required="" ${gender == -1?"checked":""}/>&nbsp;Other<br/>
                                                </div>
                                            </div>

                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="email">Email</label>
                                                    <input type="email" id="email" class="form-control" name="email" required="" value="${email}"/>
                                                </div>
                                            </div><!-- comment -->
                                            
                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="phone">Phone</label>
                                                    <input type="number" id="phone" class="form-control" name="phone" />
                                                </div>
                                            </div>
                                            
                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="address">Address</label>
                                                    <input type="text" id="address" class="form-control" name="address" />
                                                </div>
                                            </div> 

                                            <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                                                <button type="submit" class="btn btn-primary btn-lg">Register</button>
                                            </div>

                                        </form>

                                    </div>
                                    <div class="col-md-10 col-lg-6 col-xl-7 d-flex align-items-center order-1 order-lg-2">

                                        <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-registration/draw1.webp"
                                             class="img-fluid" alt="Sample image">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
